package com.colorata.animateaslifestyle

import androidx.compose.animation.core.Animatable
import androidx.compose.animation.core.AnimationState
import androidx.compose.animation.core.VectorConverter
import androidx.compose.animation.core.spring
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.geometry.Rect
import androidx.compose.ui.geometry.Size
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.IntOffset
import androidx.compose.ui.unit.IntSize
import kotlinx.coroutines.CancellationException

/**
 * [Animatable] is a value holder that automatically animates its value when the value is
 * changed via [Animatable.animateTo]. If [Animatable.animateTo] is invoked during an ongoing value change animation,
 * a new animation will transition [Animatable] from its current value (i.e. value at the point of
 * interruption) to the new [Animatable.targetValue]. This ensures that the value change is __always__
 * continuous using [Animatable.animateTo]. If a [spring] animation (e.g. default animation) is used with
 * [Animatable.animateTo], the velocity change will guarantee to be continuous as well.
 *
 * Unlike [AnimationState], [Animatable] ensures *mutual exclusiveness* on its animations. To
 * achieve this, when a new animation is started via [Animatable.animateTo] (or [Animatable.animateTo]), any ongoing
 * animation will be canceled via a [CancellationException].
 *
 * @param initialValue initial value of the animatable value holder
 * @param visibilityThreshold Threshold at which the animation may round off to its target value.
 *
 * @see Animatable.animateTo
 * @see Animatable.animateDecay
 */
fun Animatable(initialValue: Dp, visibilityThreshold: Dp? = null) =
    Animatable(initialValue, Dp.VectorConverter, visibilityThreshold)

/**
 * [Animatable] is a value holder that automatically animates its value when the value is
 * changed via [Animatable.animateTo]. If [Animatable.animateTo] is invoked during an ongoing value change animation,
 * a new animation will transition [Animatable] from its current value (i.e. value at the point of
 * interruption) to the new [Animatable.targetValue]. This ensures that the value change is __always__
 * continuous using [Animatable.animateTo]. If a [spring] animation (e.g. default animation) is used with
 * [Animatable.animateTo], the velocity change will guarantee to be continuous as well.
 *
 * Unlike [AnimationState], [Animatable] ensures *mutual exclusiveness* on its animations. To
 * achieve this, when a new animation is started via [Animatable.animateTo] (or [Animatable.animateTo]), any ongoing
 * animation will be canceled via a [CancellationException].
 *
 * @param initialValue initial value of the animatable value holder
 * @param visibilityThreshold Threshold at which the animation may round off to its target value.
 *
 * @see Animatable.animateTo
 * @see Animatable.animateDecay
 */
fun Animatable(initialValue: Size, visibilityThreshold: Size? = null) = Animatable(
    initialValue,
    Size.VectorConverter,
    visibilityThreshold
)

/**
 * [Animatable] is a value holder that automatically animates its value when the value is
 * changed via [Animatable.animateTo]. If [Animatable.animateTo] is invoked during an ongoing value change animation,
 * a new animation will transition [Animatable] from its current value (i.e. value at the point of
 * interruption) to the new [Animatable.targetValue]. This ensures that the value change is __always__
 * continuous using [Animatable.animateTo]. If a [spring] animation (e.g. default animation) is used with
 * [Animatable.animateTo], the velocity change will guarantee to be continuous as well.
 *
 * Unlike [AnimationState], [Animatable] ensures *mutual exclusiveness* on its animations. To
 * achieve this, when a new animation is started via [Animatable.animateTo] (or [Animatable.animateTo]), any ongoing
 * animation will be canceled via a [CancellationException].
 *
 * @param initialValue initial value of the animatable value holder
 * @param visibilityThreshold Threshold at which the animation may round off to its target value.
 *
 * @see Animatable.animateTo
 * @see Animatable.animateDecay
 */
fun Animatable(initialValue: Offset, visibilityThreshold: Offset? = null) =
    Animatable(initialValue, Offset.VectorConverter, visibilityThreshold)

/**
 * [Animatable] is a value holder that automatically animates its value when the value is
 * changed via [Animatable.animateTo]. If [Animatable.animateTo] is invoked during an ongoing value change animation,
 * a new animation will transition [Animatable] from its current value (i.e. value at the point of
 * interruption) to the new [Animatable.targetValue]. This ensures that the value change is __always__
 * continuous using [Animatable.animateTo]. If a [spring] animation (e.g. default animation) is used with
 * [Animatable.animateTo], the velocity change will guarantee to be continuous as well.
 *
 * Unlike [AnimationState], [Animatable] ensures *mutual exclusiveness* on its animations. To
 * achieve this, when a new animation is started via [Animatable.animateTo] (or [Animatable.animateTo]), any ongoing
 * animation will be canceled via a [CancellationException].
 *
 * @param initialValue initial value of the animatable value holder
 * @param visibilityThreshold Threshold at which the animation may round off to its target value.
 *
 * @see Animatable.animateTo
 * @see Animatable.animateDecay
 */
fun Animatable(initialValue: Rect, visibilityThreshold: Rect? = null) =
    Animatable(initialValue, Rect.VectorConverter, visibilityThreshold)

/**
 * [Animatable] is a value holder that automatically animates its value when the value is
 * changed via [Animatable.animateTo]. If [Animatable.animateTo] is invoked during an ongoing value change animation,
 * a new animation will transition [Animatable] from its current value (i.e. value at the point of
 * interruption) to the new [Animatable.targetValue]. This ensures that the value change is __always__
 * continuous using [Animatable.animateTo]. If a [spring] animation (e.g. default animation) is used with
 * [Animatable.animateTo], the velocity change will guarantee to be continuous as well.
 *
 * Unlike [AnimationState], [Animatable] ensures *mutual exclusiveness* on its animations. To
 * achieve this, when a new animation is started via [Animatable.animateTo] (or [Animatable.animateTo]), any ongoing
 * animation will be canceled via a [CancellationException].
 *
 * @param initialValue initial value of the animatable value holder
 * @param visibilityThreshold Threshold at which the animation may round off to its target value.
 *
 * @see Animatable.animateTo
 * @see Animatable.animateDecay
 */
fun Animatable(initialValue: Int, visibilityThreshold: Int? = null) = Animatable(
    initialValue,
    Int.VectorConverter,
    visibilityThreshold
)

/**
 * [Animatable] is a value holder that automatically animates its value when the value is
 * changed via [Animatable.animateTo]. If [Animatable.animateTo] is invoked during an ongoing value change animation,
 * a new animation will transition [Animatable] from its current value (i.e. value at the point of
 * interruption) to the new [Animatable.targetValue]. This ensures that the value change is __always__
 * continuous using [Animatable.animateTo]. If a [spring] animation (e.g. default animation) is used with
 * [Animatable.animateTo], the velocity change will guarantee to be continuous as well.
 *
 * Unlike [AnimationState], [Animatable] ensures *mutual exclusiveness* on its animations. To
 * achieve this, when a new animation is started via [Animatable.animateTo] (or [Animatable.animateTo]), any ongoing
 * animation will be canceled via a [CancellationException].
 *
 * @param initialValue initial value of the animatable value holder
 * @param visibilityThreshold Threshold at which the animation may round off to its target value.
 *
 * @see Animatable.animateTo
 * @see Animatable.animateDecay
 */
fun Animatable(initialValue: IntOffset, visibilityThreshold: IntOffset? = null) = Animatable(
    initialValue,
    IntOffset.VectorConverter,
    visibilityThreshold
)

/**
 * [Animatable] is a value holder that automatically animates its value when the value is
 * changed via [Animatable.animateTo]. If [Animatable.animateTo] is invoked during an ongoing value change animation,
 * a new animation will transition [Animatable] from its current value (i.e. value at the point of
 * interruption) to the new [Animatable.targetValue]. This ensures that the value change is __always__
 * continuous using [Animatable.animateTo]. If a [spring] animation (e.g. default animation) is used with
 * [Animatable.animateTo], the velocity change will guarantee to be continuous as well.
 *
 * Unlike [AnimationState], [Animatable] ensures *mutual exclusiveness* on its animations. To
 * achieve this, when a new animation is started via [Animatable.animateTo] (or [Animatable.animateTo]), any ongoing
 * animation will be canceled via a [CancellationException].
 *
 * @param initialValue initial value of the animatable value holder
 * @param visibilityThreshold Threshold at which the animation may round off to its target value.
 *
 * @see Animatable.animateTo
 * @see Animatable.animateDecay
 */
fun Animatable(initialValue: IntSize, visibilityThreshold: IntSize? = null) = Animatable(
    initialValue,
    IntSize.VectorConverter,
    visibilityThreshold
)