package com.colorata.animateaslifestyle

import androidx.compose.animation.animateColorAsState
import androidx.compose.animation.core.AnimationSpec
import androidx.compose.animation.core.spring
import androidx.compose.animation.core.tween
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.size
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.composed
import androidx.compose.ui.draw.clip
import androidx.compose.ui.draw.drawBehind
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.RectangleShape
import androidx.compose.ui.graphics.Shape
import androidx.compose.ui.graphics.drawOutline
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.Density
import androidx.compose.ui.unit.dp

/**
 * Animates background with given [color]. Animation will start when [color] was changed
 * @param color Background color of composition
 * @param animationSpec Animation spec for animation
 * @param shape shape of composition
 */
fun Modifier.animateBackground(
    color: Color, animationSpec: AnimationSpec<Color> = spring(), shape: Shape = RectangleShape
) = composed {
    val internalColor by animateColorAsState(targetValue = color, animationSpec = animationSpec)

    clip(shape).drawBehind {
        drawOutline(
            shape.createOutline(size, layoutDirection, Density(density, fontScale)), internalColor
        )
    }
}