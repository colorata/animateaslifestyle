package com.colorata.animateaslifestyle

import androidx.compose.animation.core.*
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.composed
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp

fun Modifier.animateRoundedCornerShape(percent: Int, animationSpec: AnimationSpec<Int> = spring()) =
    composed {
        clip(com.colorata.animateaslifestyle.animateRoundedCornerShape(percent, animationSpec))
    }

fun Modifier.animateRoundedCornerShape(size: Dp, animationSpec: AnimationSpec<Dp> = spring()) =
    composed {
        clip(com.colorata.animateaslifestyle.animateRoundedCornerShape(size, animationSpec))
    }


@Composable
fun animateRoundedCornerShape(
    percent: Int,
    animationSpec: AnimationSpec<Int> = spring()
): RoundedCornerShape {
    val internalPercent by animateIntAsState(targetValue = percent, animationSpec = animationSpec)
    return RoundedCornerShape(internalPercent)
}

@Composable
fun animateRoundedCornerShape(
    size: Dp,
    animationSpec: AnimationSpec<Dp> = spring()
): RoundedCornerShape {
    val internalSize by animateDpAsState(targetValue = size, animationSpec = animationSpec)
    return RoundedCornerShape(internalSize)
}