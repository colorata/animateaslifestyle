package com.colorata.animateaslifestyle

import androidx.compose.animation.core.animateFloatAsState
import androidx.compose.animation.core.animateIntOffsetAsState
import androidx.compose.animation.core.animateOffsetAsState
import androidx.compose.foundation.gestures.Orientation
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.composed
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.graphics.TransformOrigin
import androidx.compose.ui.graphics.graphicsLayer
import androidx.compose.ui.unit.IntOffset

/**
 * More efficient alternative for *AnimatedVisibility*. Only supports **Fade**, **Scale** and **Slide**.
 * @param visible Is Composition visible
 * @param transition Transition of animation. [fade] by default
 * @see Transition
 * @see fade
 * @see slide
 * @see scale
 */
fun Modifier.animateVisibility(
    visible: Boolean, transition: Transition = fade()
) = composed {
    val optimized = rememberOptimizedAnimationsFor(transition)
    val scaleX = if (optimized.containsScale()) animateFloatAsState(
        targetValue = if (visible) 1f else optimized.scale!!.from.x,
        animationSpec = optimized.scale!!.animationSpec
    ).value else 1f
    val scaleY = if (optimized.containsScale()) animateFloatAsState(
        targetValue = if (visible) 1f else optimized.scale!!.from.y,
        animationSpec = optimized.scale!!.animationSpec
    ).value else 1f
    val alpha = if (optimized.containsFade()) animateFloatAsState(
        targetValue = if (visible) 1f else optimized.fade!!.from,
        animationSpec = optimized.fade!!.animationSpec
    ).value else 1f
    val slideOffset = if (optimized.containsSlide()) animateOffsetAsState(
        targetValue = if (visible) Offset(0f, 0f) else optimized.slide!!.from,
        animationSpec = optimized.slide!!.animationSpec
    ).value else Offset(0f, 0f)
    graphicsLayer(scaleX = scaleX,
        scaleY = scaleY,
        alpha = alpha,
        translationX = slideOffset.x,
        translationY = slideOffset.y,
        transformOrigin = if (optimized.containsScale()) remember {
            optimized.scale?.transformOrigin ?: TransformOrigin.Center
        } else TransformOrigin.Center)
}