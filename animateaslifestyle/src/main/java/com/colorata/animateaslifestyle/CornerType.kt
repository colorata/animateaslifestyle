package com.colorata.animateaslifestyle

import androidx.compose.foundation.shape.CutCornerShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp

enum class CornerType {
    Rounded, Cut
}

fun CornerShape(percent: Int, cornerType: CornerType = CornerType.Rounded) =
    if (cornerType == CornerType.Rounded) RoundedCornerShape(percent)
    else CutCornerShape(percent)

fun CornerShape(
    topStartPercent: Int = 0,
    topEndPercent: Int = 0,
    bottomEndPercent: Int = 0,
    bottomStartPercent: Int = 0,
    cornerType: CornerType = CornerType.Rounded
) =
    if (cornerType == CornerType.Rounded) RoundedCornerShape(
        topStartPercent = topStartPercent,
        topEndPercent = topEndPercent,
        bottomEndPercent = bottomEndPercent,
        bottomStartPercent = bottomStartPercent
    )
    else CutCornerShape(
        topStartPercent = topStartPercent,
        topEndPercent = topEndPercent,
        bottomEndPercent = bottomEndPercent,
        bottomStartPercent = bottomStartPercent
    )

fun CornerShape(
    topStart: Dp = 0.dp,
    topEnd: Dp = 0.dp,
    bottomEnd: Dp = 0.dp,
    bottomStart: Dp = 0.dp, cornerType: CornerType = CornerType.Rounded
) =
    if (cornerType == CornerType.Rounded) RoundedCornerShape(
        topStart = topStart,
        topEnd = topEnd,
        bottomEnd = bottomEnd,
        bottomStart = bottomStart
    )
    else CutCornerShape(
        topStart = topStart,
        topEnd = topEnd,
        bottomEnd = bottomEnd,
        bottomStart = bottomStart
    )

fun CornerShape(
    topStart: Float = 0.0f,
    topEnd: Float = 0.0f,
    bottomEnd: Float = 0.0f,
    bottomStart: Float = 0.0f, cornerType: CornerType = CornerType.Rounded
) =
    if (cornerType == CornerType.Rounded) RoundedCornerShape(
        topStart = topStart,
        topEnd = topEnd,
        bottomEnd = bottomEnd,
        bottomStart = bottomStart
    )
    else CutCornerShape(
        topStart = topStart,
        topEnd = topEnd,
        bottomEnd = bottomEnd,
        bottomStart = bottomStart
    )

fun CornerShape(size: Float, cornerType: CornerType = CornerType.Rounded) =
    if (cornerType == CornerType.Rounded) RoundedCornerShape(size)
    else CutCornerShape(size)

fun CornerShape(size: Dp, cornerType: CornerType = CornerType.Rounded) =
    if (cornerType == CornerType.Rounded) RoundedCornerShape(size)
    else CutCornerShape(size)
