package com.colorata.animateaslifestyle

import androidx.compose.runtime.*
import kotlinx.coroutines.delay

/**
 * Function to get is composition actually launched.
 * @param delay delay when composition launches and this function returns true. 0 by default
 * @return is composition launched.
 */
@Composable
fun isCompositionLaunched(delay: Number = 0): Boolean {
    var launched by remember { mutableStateOf(false) }
    LaunchedEffect(key1 = true) {
        delay(delay.toLong())
        launched = true
    }
    return launched
}