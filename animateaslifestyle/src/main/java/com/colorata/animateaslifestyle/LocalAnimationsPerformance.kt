package com.colorata.animateaslifestyle

import androidx.compose.runtime.Composable
import androidx.compose.runtime.CompositionLocalProvider
import androidx.compose.runtime.compositionLocalOf
import androidx.compose.runtime.remember


/**
 * Enum class for defining animation performance
 */
enum class AnimationsPerformance(val maxWeight: Int) {
    /**
     * Max performance for animations. All animations are allowed
     */
    Full(Int.MAX_VALUE),

    /**
     * Simplified animations. Allowed animations: [fade] + [scale], [slide] or lower
     */
    Simplified(3),

    /**
     * Super simplified animations. [fade] only allowed
     */
    SuperSimplified(1),

    /**
     * No animations at all allowed
     */
    None(0)
}

/**
 * [AnimationsPerformance] provider for Jetpack Compose. [AnimationsPerformance.Full] by default
 */
val LocalAnimationsPerformance = compositionLocalOf { AnimationsPerformance.Full }

/**
 * Provides animations performance for Composition
 * @param animationsPerformance [AnimationsPerformance] that will be used in Composition
 * @param content Composition
 */
@Composable
fun ProvideAnimationsPerformance(
    animationsPerformance: AnimationsPerformance = AnimationsPerformance.Full,
    content: @Composable () -> Unit
) {
    CompositionLocalProvider(LocalAnimationsPerformance provides animationsPerformance) {
        content()
    }
}

/**
 * Optimizes performance of given animation to correctly show in low-budget devices(or old ones).
 * @param transition transition that you want to optimize.
 * @return optimized animation.
 */
@Composable
fun rememberOptimizedAnimationsFor(transition: Transition): Transition {
    val maxWeight = LocalAnimationsPerformance.current.maxWeight
    return remember(transition, maxWeight) {
        transition.optimized(maxWeight)
    }
}
