package com.colorata.animateaslifestyle

import androidx.compose.ui.Modifier
import androidx.compose.ui.composed
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.graphics.graphicsLayer
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.DpOffset
import androidx.compose.ui.unit.dp
import com.colorata.animateaslifestyle.stagger.toPx

fun Modifier.graphicsOffset(offset: Offset) = graphicsLayer(
    translationX = offset.x,
    translationY = offset.y
)

fun Modifier.graphicsOffset(offset: DpOffset) = composed {
    graphicsLayer(
        translationX = offset.x.toPx(),
        translationY = offset.y.toPx()
    )
}

fun Modifier.graphicsOffset(x: Float = 0f, y: Float = 0f) = graphicsLayer(
    translationX = x,
    translationY = y
)

fun Modifier.graphicsOffset(x: Dp = 0.dp, y: Dp = 0.dp) = composed {
    graphicsLayer(
        translationX = x.toPx(),
        translationY = y.toPx()
    )
}