package com.colorata.animateaslifestyle

import androidx.compose.animation.*
import androidx.compose.animation.core.FiniteAnimationSpec
import androidx.compose.animation.core.spring
import androidx.compose.runtime.Immutable
import androidx.compose.ui.Alignment
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.graphics.TransformOrigin
import androidx.compose.ui.unit.IntOffset

/**
 * Class to accumulate transitions
 * @param slide Slide transition
 * @param fade Fade transition
 * @param scale Scale transition
 */
@Immutable
data class Transition(
    val slide: Slide? = null,
    val fade: Fade? = null,
    val scale: Scale? = null
) {
    /**
     * "Overlays" second transition over first. Null is used as "Alpha" channel
     */
    operator fun plus(other: Transition) =
        copy(slide = other.slide ?: slide, fade = other.fade ?: fade, scale = other.scale ?: scale)
}

/**
 * Calculates weights for given [Transition]
 */
fun Transition.weights() =
    (if (containsFade()) 1 else 0) + (if (containsScale()) 2 else 0) + (if (containsSlide()) 3 else 0)

/**
 * Downgrades [Transition] ***once*** to lower level
 * @param animationSpec General animation spec for transition
 */
fun Transition.downgrade(animationSpec: FiniteAnimationSpec<Float> = spring()) =
    when {
        fade != null && slide != null -> copy(
            slide = null,
            scale = scaleTransitionSpec(animationSpec = animationSpec),
            fade = fade
        )
        fade != null && scale != null -> copy(slide = null, fade = null, scale = scale)
        scale != null -> copy(
            slide = null,
            scale = null,
            fade = fadeTransitionSpec(animationSpec = animationSpec)
        )
        else -> copy(
            slide = null,
            scale = null,
            fade = fadeTransitionSpec(animationSpec = animationSpec)
        )
    }

/**
 * Function to get is [Transition] contains scale
 */
fun Transition.containsScale() = scale != null

/**
 * Function to get is [Transition] contains slide
 */
fun Transition.containsSlide() = slide != null

/**
 * Function to get is [Transition] contains fade
 */
fun Transition.containsFade() = fade != null

/**
 * Function to get is [Transition] contains ***only*** scale
 */
fun Transition.containsOnlyScale() = fade == null && slide == null && scale != null

/**
 * Function to get is [Transition] contains ***only*** fade
 */
fun Transition.containsOnlyFade() = scale == null && slide == null && fade != null

/**
 * Optimizes [Transition] to match given [maxWeight]
 * @param maxWeight Max weight for animations
 * @return Optimized animation
 * @see AnimationsPerformance
 */
fun Transition.optimized(maxWeight: Int): Transition {
    var currentAnimations = this
    while (currentAnimations.weights() > maxWeight && currentAnimations.containsOnlyFade()) {
        currentAnimations = currentAnimations.downgrade()
    }
    return currentAnimations
}

fun Alignment.toTransformOrigin(padding: Float = 0f) = when (this) {
    Alignment.TopStart -> TransformOrigin(0f + padding, 0f + padding)
    Alignment.TopCenter -> TransformOrigin(0.5f, 0f + padding)
    Alignment.TopEnd -> TransformOrigin(0.5f, 1f - padding)
    Alignment.CenterStart -> TransformOrigin(0f + padding, 0.5f)
    Alignment.Center -> TransformOrigin(0.5f, 0.5f)
    Alignment.CenterEnd -> TransformOrigin(1f - padding, 0.5f)
    Alignment.BottomStart -> TransformOrigin(0f + padding, 1f - padding)
    Alignment.BottomCenter -> TransformOrigin(0.5f, 1f - padding)
    Alignment.BottomEnd -> TransformOrigin(1f - padding, 1f - padding)
    else -> TransformOrigin(0.5f, 0.5f)
}


/**
 * Creates transition for given [slide], [fade] and [scale]
 * @param slide Slide transition
 * @param fade Fade transition
 * @param scale Scale transition
 */
fun transition(
    slide: Slide? = null,
    fade: Fade? = null,
    scale: Scale? = null
) = Transition(slide, fade, scale)

@OptIn(ExperimentalAnimationApi::class)
fun Transition.toEnterTransition(): EnterTransition {
    var transition: EnterTransition = fadeIn()
    if (fade != null) transition += fadeIn(animationSpec = fade.animationSpec, fade.from)
    if (scale != null) transition += scaleIn(
        animationSpec = scale.animationSpec,
        minOf(scale.from.x, scale.from.y)
    )
    if (slide != null) transition += slideIn(
        slide.animationSpec.asOther()
    ) { IntOffset(slide.from.x.toInt(), slide.from.y.toInt()) }
    return transition
}

@OptIn(ExperimentalAnimationApi::class)
fun Transition.toExitTransition(): ExitTransition {
    var transition: ExitTransition = fadeOut()
    if (fade != null) transition += fadeOut(animationSpec = fade.animationSpec, fade.from)
    if (scale != null) transition += scaleOut(
        animationSpec = scale.animationSpec,
        minOf(scale.from.x, scale.from.y)
    )
    if (slide != null) transition += slideOut(
        slide.animationSpec.asOther()
    ) { IntOffset(slide.from.x.toInt(), slide.from.y.toInt()) }
    return transition
}

val Transition.enterTransition: EnterTransition
    get() = toEnterTransition()

val Transition.exitTransition: ExitTransition
    get() = toExitTransition()

@OptIn(ExperimentalAnimationApi::class)
infix fun Transition.with(other: Transition): ContentTransform =
    this.toEnterTransition() with other.toExitTransition()