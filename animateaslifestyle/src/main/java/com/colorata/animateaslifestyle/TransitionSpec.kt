package com.colorata.animateaslifestyle

import androidx.compose.animation.core.AnimationSpec
import androidx.compose.animation.core.FiniteAnimationSpec
import androidx.compose.animation.core.spring
import androidx.compose.runtime.Immutable
import androidx.compose.ui.Alignment
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.graphics.TransformOrigin

@Immutable
data class Slide(
    val from: Offset,
    val animationSpec: FiniteAnimationSpec<Offset>
)

data class ScaleOffset(
    val x: Float = 1f,
    val y: Float = x
)

@Immutable
data class Scale(
    val from: ScaleOffset,
    val animationSpec: FiniteAnimationSpec<Float>,
    val transformOrigin: TransformOrigin
)

@Immutable
data class Fade(
    val from: Float,
    val animationSpec: FiniteAnimationSpec<Float>
)

/**
 * Creates Fade
 * @param from Alpha value when element is hidden
 * @param animationSpec Animation spec for animation
 */
fun fade(
    from: Float = 0f,
    animationSpec: FiniteAnimationSpec<Float> = spring()
) = transition(fade = fadeTransitionSpec(from, animationSpec))

/**
 * Creates Fade
 * @param from Alpha value when element is hidden
 * @param animationSpec Animation spec for animation
 */
fun fadeTransitionSpec(
    from: Float = 0f,
    animationSpec: FiniteAnimationSpec<Float> = spring()
) = Fade(from, animationSpec)

/**
 * Creates Slide [Transition]
 * @param from Slide value when element is hidden
 * @param to Slide value when element is visible
 * @param animationSpec Animation spec for animation
 * @param orientation Orientation of slide animation
 */
fun slide(
    from: Offset = Offset(100f, 100f),
    animationSpec: FiniteAnimationSpec<Offset> = spring()
) = transition(slide = slideTransitionSpec(from, animationSpec))

fun slideHorizontally(
    from: Float = 100f,
    animationSpec: FiniteAnimationSpec<Offset> = spring()
) = slide(Offset(from, 0f), animationSpec)

fun slideVertically(
    from: Float = 100f,
    animationSpec: FiniteAnimationSpec<Offset> = spring()
) = slide(Offset(0f, from), animationSpec)

/**
 * Creates Slide
 * @param from Slide value when element is hidden
 * @param animationSpec Animation spec for animation
 */
fun slideTransitionSpec(
    from: Offset = Offset(100f, 100f),
    animationSpec: FiniteAnimationSpec<Offset> = spring()
) = Slide(from, animationSpec)

/**
 * Creates Scale [Transition]
 * @param from Scale value when element is hidden
 * @param animationSpec Animation spec for animation
 */
fun scale(
    from: ScaleOffset = ScaleOffset(0f, 0f),
    animationSpec: FiniteAnimationSpec<Float> = spring(),
    transformOrigin: TransformOrigin = Alignment.Center.toTransformOrigin()
) = transition(scale = scaleTransitionSpec(from, animationSpec, transformOrigin))

fun scale(
    from: Float = 0f,
    animationSpec: FiniteAnimationSpec<Float> = spring(),
    transformOrigin: TransformOrigin = Alignment.Center.toTransformOrigin()
) = transition(scale = scaleTransitionSpec(ScaleOffset(from), animationSpec, transformOrigin))

fun scaleHorizontally(
    from: Float = 0f,
    animationSpec: FiniteAnimationSpec<Float> = spring(),
    transformOrigin: TransformOrigin = Alignment.Center.toTransformOrigin()
) = scale(ScaleOffset(from, 1f), animationSpec, transformOrigin)

fun scaleVertically(
    from: Float = 0f,
    animationSpec: FiniteAnimationSpec<Float> = spring(),
    transformOrigin: TransformOrigin = Alignment.Center.toTransformOrigin()
) = scale(ScaleOffset(1f, from), animationSpec, transformOrigin)

/**
 * Creates Scale
 * @param from Scale value when element is hidden
 * @param animationSpec Animation spec for animation
 */
fun scaleTransitionSpec(
    from: ScaleOffset = ScaleOffset(0f, 0f),
    animationSpec: FiniteAnimationSpec<Float> = spring(),
    transformOrigin: TransformOrigin = Alignment.Center.toTransformOrigin()
) = Scale(from, animationSpec, transformOrigin)

@Suppress("UNCHECKED_CAST")
fun <T, V> AnimationSpec<T>.asOther(): AnimationSpec<V> = this as AnimationSpec<V>

@Suppress("UNCHECKED_CAST")
fun <T, V> FiniteAnimationSpec<T>.asOther(): FiniteAnimationSpec<V> = this as FiniteAnimationSpec<V>
