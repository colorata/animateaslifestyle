package com.colorata.animateaslifestyle.material3

import androidx.compose.foundation.background
import androidx.compose.foundation.gestures.Orientation
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.LazyItemScope
import androidx.compose.foundation.lazy.LazyListScope
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.material3.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.Shape
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import com.colorata.animateaslifestyle.CornerShape
import com.colorata.animateaslifestyle.CornerType
import com.colorata.animateaslifestyle.material3.GroupedItemsUtils.horizontalCorners
import com.colorata.animateaslifestyle.material3.GroupedItemsUtils.itemPositionFromIndex
import com.colorata.animateaslifestyle.material3.GroupedItemsUtils.maxCorner
import com.colorata.animateaslifestyle.material3.GroupedItemsUtils.minCorner
import com.colorata.animateaslifestyle.material3.GroupedItemsUtils.padding
import com.colorata.animateaslifestyle.material3.GroupedItemsUtils.verticalCorners

typealias ColorProvider<T> = @Composable (index: Int, item: T) -> Color

object GroupedItemsUtils {
    val minCorner = 2.dp
    val maxCorner = 10.dp
    val padding = 2.dp

    fun itemPositionFromIndex(index: Int, maxIndex: Int) = when (index) {
        0 -> ItemPosition.Start
        maxIndex -> ItemPosition.End
        else -> ItemPosition.Center
    }

    fun verticalCorners(
        itemPosition: ItemPosition,
        outerCorner: Dp = maxCorner,
        innerCorner: Dp = minCorner,
        cornerType: CornerType = CornerType.Rounded
    ) = when (itemPosition) {
        ItemPosition.Start -> CornerShape(
            outerCorner,
            outerCorner,
            innerCorner,
            innerCorner,
            cornerType
        )

        ItemPosition.End -> CornerShape(
            innerCorner,
            innerCorner,
            outerCorner,
            outerCorner,
            cornerType
        )

        ItemPosition.Center -> CornerShape(innerCorner, cornerType)
    }

    fun horizontalCorners(
        itemPosition: ItemPosition,
        outerCorner: Dp = maxCorner,
        innerCorner: Dp = minCorner,
        cornerType: CornerType = CornerType.Rounded
    ) = when (itemPosition) {
        ItemPosition.Start -> CornerShape(
            outerCorner,
            innerCorner,
            innerCorner,
            outerCorner,
            cornerType
        )

        ItemPosition.End -> CornerShape(
            innerCorner,
            outerCorner,
            outerCorner,
            innerCorner,
            cornerType
        )

        ItemPosition.Center -> CornerShape(innerCorner, cornerType)
    }
}

enum class ItemPosition {
    Start, Center, End
}

@Composable
private fun <T> GroupedItemsContent(
    items: List<T>,
    colorProvider: ColorProvider<T>,
    corners: (position: ItemPosition) -> Shape,
    content: @Composable (item: T) -> Unit
) {
    items.forEachIndexed { index, it ->
        Box(
            modifier = Modifier
                .clip(corners(remember {
                    itemPositionFromIndex(
                        index, items.lastIndex
                    )
                }))
                .background(colorProvider(index, it))
        ) {
            content(it)
        }
    }
}

@Composable
private fun <T> InternalGroupedRow(
    items: List<T>,
    modifier: Modifier,
    horizontalArrangement: Arrangement.Horizontal,
    verticalAlignment: Alignment.Vertical,
    colors: @Composable (index: Int, item: T) -> Color,
    corners: (position: ItemPosition) -> Shape,
    content: @Composable (item: T) -> Unit
) {
    Row(
        modifier, horizontalArrangement, verticalAlignment
    ) {
        GroupedItemsContent(items, colors, corners, content)
    }
}

@Composable
private fun <T> InternalGroupedColumn(
    items: List<T>,
    modifier: Modifier,
    verticalArrangement: Arrangement.Vertical,
    horizontalAlignment: Alignment.Horizontal,
    colorProvider: @Composable (index: Int, item: T) -> Color,
    corners: (position: ItemPosition) -> Shape,
    content: @Composable (item: T) -> Unit
) {
    Column(
        modifier, verticalArrangement, horizontalAlignment
    ) {
        GroupedItemsContent(items, colorProvider, corners, content)
    }
}

@Composable
fun <T> GroupedRow(
    items: List<T>,
    modifier: Modifier = Modifier,
    horizontalArrangement: Arrangement.Horizontal = Arrangement.spacedBy(padding),
    verticalAlignment: Alignment.Vertical = Alignment.CenterVertically,
    colors: ColorProvider<T> = { _, _ -> MaterialTheme.colorScheme.surfaceVariant },
    corners: (position: ItemPosition) -> Shape = {
        horizontalCorners(
            it
        )
    },
    content: @Composable (item: T) -> Unit
) {
    InternalGroupedRow(
        items = items,
        modifier = modifier,
        horizontalArrangement = horizontalArrangement,
        verticalAlignment = verticalAlignment,
        colors = colors,
        corners = corners
    ) {
        content(it)
    }
}

@Composable
fun <T> GroupedColumn(
    items: List<T>,
    modifier: Modifier = Modifier,
    verticalArrangement: Arrangement.Vertical = Arrangement.spacedBy(padding),
    horizontalAlignment: Alignment.Horizontal = Alignment.CenterHorizontally,
    colorProvider: ColorProvider<T> = { _, _ -> MaterialTheme.colorScheme.surfaceVariant },
    corners: (position: ItemPosition) -> Shape = {
        verticalCorners(
            it
        )
    },
    content: @Composable (item: T) -> Unit
) {
    InternalGroupedColumn(
        items = items,
        modifier = modifier,
        verticalArrangement = verticalArrangement,
        horizontalAlignment = horizontalAlignment,
        colorProvider = colorProvider,
        corners = corners
    ) {
        content(it)
    }
}

@Composable
fun <T> GroupedRow(
    items: List<T>,
    modifier: Modifier = Modifier,
    horizontalArrangement: Arrangement.Horizontal = Arrangement.spacedBy(padding),
    verticalAlignment: Alignment.Vertical = Alignment.CenterVertically,
    color: Color = MaterialTheme.colorScheme.surfaceVariant,
    outerCorner: Dp = maxCorner,
    innerCorner: Dp = minCorner,
    cornerType: CornerType = CornerType.Rounded,
    content: @Composable (item: T) -> Unit
) {
    InternalGroupedRow(items = items,
        modifier = modifier,
        horizontalArrangement = horizontalArrangement,
        verticalAlignment = verticalAlignment,
        colors = { _, _ -> color },
        corners = {
            horizontalCorners(it, outerCorner, innerCorner, cornerType = cornerType)
        }) {
        content(it)
    }
}

@Composable
fun <T> GroupedColumn(
    items: List<T>,
    modifier: Modifier = Modifier,
    verticalArrangement: Arrangement.Vertical = Arrangement.spacedBy(padding),
    horizontalAlignment: Alignment.Horizontal = Alignment.CenterHorizontally,
    color: Color = MaterialTheme.colorScheme.surfaceVariant,
    outerCorner: Dp = maxCorner,
    innerCorner: Dp = minCorner,
    cornerType: CornerType = CornerType.Rounded,
    content: @Composable (item: T) -> Unit
) {
    InternalGroupedColumn(
        items = items,
        modifier = modifier,
        verticalArrangement = verticalArrangement,
        horizontalAlignment = horizontalAlignment,
        colorProvider = { _, _ -> color },
        corners = { verticalCorners(it, outerCorner, innerCorner, cornerType) }
    ) {
        content(it)
    }
}

inline fun <T> LazyListScope.groupedItems(
    items: List<T>,
    orientation: Orientation,
    padding: Dp = GroupedItemsUtils.padding,
    crossinline colorProvider: ColorProvider<T> = { _, _ -> MaterialTheme.colorScheme.surfaceVariant },
    crossinline corners: (position: ItemPosition) -> Shape,
    noinline key: ((item: T) -> Any)? = null,
    noinline contentType: (item: T) -> Any? = { null },
    crossinline itemContent: @Composable LazyItemScope.(item: T) -> Unit
) {
    itemsIndexed(
        items,
        key = if (key != null) { _, item -> key(item) } else null,
        contentType = { _, item ->
            contentType(item)
        }) { index, it ->
        if (orientation == Orientation.Vertical) {
            Column {
                Column(
                    Modifier
                        .clip(corners(remember {
                            itemPositionFromIndex(
                                index, items.lastIndex
                            )
                        }))
                        .background(colorProvider(index, it))
                ) {
                    itemContent(it)
                }
                if (index < items.lastIndex) {
                    Spacer(modifier = Modifier.height(padding))
                }
            }
        } else {
            Row {
                Row(
                    Modifier
                        .clip(corners(remember {
                            itemPositionFromIndex(
                                index, items.lastIndex
                            )
                        }))
                        .background(colorProvider(index, it))
                ) {
                    itemContent(it)
                }
                if (index < items.lastIndex) {
                    Spacer(modifier = Modifier.width(padding))
                }
            }
        }
    }
}

inline fun <T> LazyListScope.groupedItems(
    items: List<T>,
    orientation: Orientation,
    padding: Dp = GroupedItemsUtils.padding,
    outerCorner: Dp = maxCorner,
    innerCorner: Dp = minCorner,
    crossinline colorProvider: ColorProvider<T> = { _, _ -> MaterialTheme.colorScheme.surfaceVariant },
    cornerType: CornerType = CornerType.Rounded,
    noinline key: ((item: T) -> Any)? = null,
    noinline contentType: (item: T) -> Any? = { null },
    crossinline itemContent: @Composable LazyItemScope.(item: T) -> Unit
) {
    groupedItems(
        items,
        orientation,
        padding,
        colorProvider,
        { position: ItemPosition ->
            if (orientation == Orientation.Horizontal) horizontalCorners(
                position, outerCorner, innerCorner, cornerType
            ) else verticalCorners(position, outerCorner, innerCorner, cornerType)
        },
        key,
        contentType,
        itemContent
    )
}