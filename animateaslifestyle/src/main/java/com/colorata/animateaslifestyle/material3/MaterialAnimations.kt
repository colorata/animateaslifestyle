@file:Suppress("unused")

package com.colorata.animateaslifestyle.material3

import androidx.compose.animation.core.*
import androidx.compose.material3.MaterialTheme
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import com.colorata.animateaslifestyle.fade
import com.colorata.animateaslifestyle.scale
import com.colorata.animateaslifestyle.slideHorizontally
import com.colorata.animateaslifestyle.slideVertically


object MaterialAnimations {
    private const val defaultMotionDuration: Int = 300
    private const val defaultFadeInDuration: Int = 150
    private const val defaultFadeOutDuration: Int = 75
    private val defaultSlideDistance: Dp = 30.dp

    private const val progressThreshold = 0.35f

    private val Int.forOutgoing: Int
        get() = (this * progressThreshold).toInt()

    private val Int.forIncoming: Int
        get() = this - this.forOutgoing

    fun sharedAxisX(
        visible: Boolean,
        forward: Boolean = true,
        slideDistance: Float = 100f,
        durationMillis: Int = defaultMotionDuration
    ) = slideHorizontally(
        when {
            visible -> if (forward) -slideDistance else slideDistance
            else -> if (forward) slideDistance else -slideDistance
        },
        animationSpec = tween(durationMillis, easing = FastOutSlowInEasing)
    ) + intFade(visible, durationMillis)

    fun sharedAxisY(
        visible: Boolean,
        forward: Boolean = true,
        slideDistance: Float = 100f,
        durationMillis: Int = defaultMotionDuration
    ) = slideVertically(
        when {
            visible -> if (forward) -slideDistance else slideDistance
            else -> if (forward) slideDistance else -slideDistance
        },
        animationSpec = tween(durationMillis, easing = FastOutSlowInEasing)
    ) + intFade(visible, durationMillis)

    fun sharedAxisZ(
        visible: Boolean,
        forward: Boolean = true,
        durationMillis: Int = defaultMotionDuration
    ) = scale(
        when {
            visible -> if (forward) 0.8f else 1.1f
            else -> if (forward) 1.1f else 0.8f
        },
        animationSpec = tween(durationMillis, easing = FastOutSlowInEasing)
    ) + intFade(visible, durationMillis)

    private fun intFade(
        visible: Boolean,
        durationMillis: Int = defaultMotionDuration
    ) = fade(
        animationSpec = tween(
            if (visible) durationMillis.forIncoming else durationMillis.forOutgoing,
            if (visible) durationMillis.forOutgoing else 0,
            if (visible) LinearOutSlowInEasing else FastOutLinearInEasing
        )
    )

    fun fadeThough(
        visible: Boolean,
        initialScale: Float = 0.92f,
        durationMillis: Int = defaultMotionDuration
    ) = intFade(
        visible,
        durationMillis
    ).let {
        if (visible) it + scale(
            initialScale,
            animationSpec = tween(
                durationMillis.forIncoming,
                durationMillis.forOutgoing,
                LinearOutSlowInEasing
            )
        ) else it
    }

    fun fade(
        visible: Boolean,
        durationMillis: Int = defaultMotionDuration
    ) = intFade(visible, durationMillis).let {
        if (visible) it + scale(
            0.8f,
            animationSpec = tween(durationMillis, easing = FastOutSlowInEasing)
        ) else it
    }

    fun elevationScale(
        alpha: Float = 0.85f,
        scale: Float = 0.85f,
        durationMillis: Int = defaultMotionDuration
    ) = fade(alpha, animationSpec = tween(durationMillis, easing = LinearEasing)) + scale(
        scale,
        animationSpec = tween(durationMillis, easing = FastOutSlowInEasing)
    )
}