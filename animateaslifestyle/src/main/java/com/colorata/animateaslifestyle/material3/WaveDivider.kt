package com.colorata.animateaslifestyle.material3

import androidx.annotation.FloatRange
import androidx.compose.animation.core.*
import androidx.compose.foundation.Canvas
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.material3.MaterialTheme
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.StrokeCap
import androidx.compose.ui.graphics.drawscope.Stroke
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import com.colorata.animateaslifestyle.shapes.ExperimentalShapeApi
import com.colorata.animateaslifestyle.material3.shapes.MaterialPaths
import com.colorata.animateaslifestyle.shapes.pi
import com.colorata.animateaslifestyle.material3.shapes.wavePath

@ExperimentalShapeApi
@Composable
fun WaveDivider(
    modifier: Modifier = Modifier,
    color: Color = MaterialTheme.colorScheme.secondary,
    animate: Boolean = true,
    withWave: Boolean = true,
    frequency: Float = 2f,
    density: Float = 10f,
    sinWidth: Dp = 3.dp,
    durationMillis: Int = 3000,
    @FloatRange(from = 0.0, to = 100.0) progress: Float = 100f
) {
    val offsetAnim by rememberInfiniteTransition().animateFloat(
        initialValue = 0f,
        targetValue = pi * 2,
        animationSpec = infiniteRepeatable(
            animation = tween(durationMillis, easing = LinearEasing)
        )
    )
    val finalOffsetAnim =
        if (animate) offsetAnim else remember { offsetAnim }
    val waveAmplitude by animateFloatAsState(targetValue = if (withWave) 1f else 0f)
    Box(
        modifier = modifier, contentAlignment = Alignment.Center
    ) {
        Canvas(
            modifier = Modifier
                .height(30.dp)
                .fillMaxWidth()
        ) {
            drawPath(
                path = MaterialPaths.wavePath(
                    size.copy(
                        width = progress / 100f * size.width - sinWidth.toPx(),
                        height = size.height - sinWidth.toPx()
                    ),
                    animOffset = finalOffsetAnim,
                    offset = Offset(sinWidth.toPx() / 2, y = sinWidth.toPx() / 2),
                    density = density,
                    waveMultiplier = frequency,
                    functionMultiplier = waveAmplitude
                ), color = color, style = Stroke(width = sinWidth.toPx(), cap = StrokeCap.Round)
            )
        }
    }
}