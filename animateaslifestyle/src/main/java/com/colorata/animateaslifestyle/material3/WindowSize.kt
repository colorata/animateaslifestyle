package com.colorata.animateaslifestyle.material3

import androidx.compose.material3.windowsizeclass.ExperimentalMaterial3WindowSizeClassApi
import androidx.compose.material3.windowsizeclass.WindowSizeClass
import androidx.compose.material3.windowsizeclass.WindowWidthSizeClass
import androidx.compose.runtime.Composable
import androidx.compose.runtime.CompositionLocalProvider
import androidx.compose.runtime.compositionLocalOf
import androidx.compose.runtime.remember
import androidx.compose.ui.platform.LocalConfiguration
import androidx.compose.ui.platform.LocalInspectionMode
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.DpSize
import androidx.compose.ui.unit.dp

/**
 * Remembers [WindowSizeClass] in Compose way. Also supports [LocalInspectionMode] and [Preview]
 * 
 * Read guidelines how to adapt your app for large screens: [Material Guidelines](https://m3.material.io/foundations/adaptive-design/large-screens/overview)
 */
@OptIn(ExperimentalMaterial3WindowSizeClassApi::class)
@Composable
fun rememberWindowSize() =
    with(LocalConfiguration.current) {
        remember {
            WindowSizeClass.calculateFromSize(
                DpSize(screenWidthDp.dp, screenHeightDp.dp)
            )
        }
    }

/**
 * Returns is current screen compact
 * Check out [Material Guidelines](https://m3.material.io/foundations/adaptive-design/large-screens/overview) for details
 */
fun WindowSizeClass.isCompact() = this.widthSizeClass == WindowWidthSizeClass.Compact

/**
 * Returns is current screen ***NOT*** compact
 * Check out [Material Guidelines](https://m3.material.io/foundations/adaptive-design/large-screens/overview) for details
 */
fun WindowSizeClass.isNotCompact() = this.widthSizeClass != WindowWidthSizeClass.Compact

/**
 * Returns is current screen medium
 * Check out [Material Guidelines](https://m3.material.io/foundations/adaptive-design/large-screens/overview) for details
 */
fun WindowSizeClass.isMedium() = this.widthSizeClass == WindowWidthSizeClass.Medium

/**
 * Returns is current screen ***NOT*** medium
 * Check out [Material Guidelines](https://m3.material.io/foundations/adaptive-design/large-screens/overview) for details
 */
fun WindowSizeClass.isNotMedium() = this.widthSizeClass != WindowWidthSizeClass.Medium

/**
 * Returns is current screen expanded
 * Check out [Material Guidelines](https://m3.material.io/foundations/adaptive-design/large-screens/overview) for details
 */
fun WindowSizeClass.isExpanded() = this.widthSizeClass == WindowWidthSizeClass.Expanded

/**
 * Returns is current screen ***NOT*** expanded
 * Check out [Material Guidelines](https://m3.material.io/foundations/adaptive-design/large-screens/overview) for details
 */
fun WindowSizeClass.isNotExpanded() = this.widthSizeClass != WindowWidthSizeClass.Expanded

/**
 * Returns current screen configuration
 * Check out [Material Guidelines](https://m3.material.io/foundations/adaptive-design/large-screens/overview) for details
 */
@OptIn(ExperimentalMaterial3WindowSizeClassApi::class)
val LocalWindowSize = compositionLocalOf {
    WindowSizeClass.calculateFromSize(
        DpSize(0.dp, 0.dp)
    )
}

/**
 * Provides screen configuration to [content]
 */
@Composable
fun ProvideWindowSize(
    content: @Composable () -> Unit
) =
    CompositionLocalProvider(LocalWindowSize provides rememberWindowSize(), content = content)