package com.colorata.animateaslifestyle.material3.fab

import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.Shape
import androidx.compose.ui.unit.dp
import com.colorata.animateaslifestyle.material3.fab.FabSize

@Composable
internal fun ActualFloatingActionButton(
    size: FabSize,
    onClick: () -> Unit,
    modifier: Modifier = Modifier,
    interactionSource: MutableInteractionSource = remember { MutableInteractionSource() },
    shape: Shape = RoundedCornerShape(
        when (size) {
            FabSize.Normal -> 16.dp
            FabSize.Large -> 28.dp
            FabSize.Extended -> 16.dp
            else -> 12.dp
        }
    ),
    containerColor: Color = MaterialTheme.colorScheme.primaryContainer,
    contentColor: Color = contentColorFor(containerColor),
    elevation: FloatingActionButtonElevation = FloatingActionButtonDefaults.elevation(),
    content: @Composable () -> Unit
) {
    when (size) {
        FabSize.Large -> {
            LargeFloatingActionButton(
                onClick, modifier, shape, containerColor, contentColor, elevation, interactionSource
            ) {
                content()
            }
        }
        FabSize.Extended -> {
            ExtendedFloatingActionButton(
                onClick, modifier, shape, containerColor, contentColor, elevation, interactionSource
            ) {
                content()
            }
        }
        FabSize.Normal -> {
            FloatingActionButton(
                onClick, modifier, shape, containerColor, contentColor, elevation, interactionSource
            ) {
                content()
            }
        }
        FabSize.Small -> {
            SmallFloatingActionButton(
                onClick, modifier, shape, containerColor, contentColor, elevation, interactionSource
            ) {
                content()
            }
        }
    }
}

