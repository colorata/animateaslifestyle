package com.colorata.animateaslifestyle.material3.fab

import androidx.compose.animation.core.tween
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.RowScope
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.FloatingActionButtonDefaults
import androidx.compose.material3.FloatingActionButtonElevation
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.contentColorFor
import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.Shape
import androidx.compose.ui.unit.dp
import com.colorata.animateaslifestyle.animateVisibility
import com.colorata.animateaslifestyle.isCompositionLaunched
import com.colorata.animateaslifestyle.scale
import com.colorata.animateaslifestyle.toTransformOrigin

@Composable
fun AnimatedFloatingActionButton(
    onClick: () -> Unit,
    modifier: Modifier = Modifier,
    size: FabSize = FabSize.Normal,
    interactionSource: MutableInteractionSource = remember { MutableInteractionSource() },
    shape: Shape = RoundedCornerShape(
        when (size) {
            FabSize.Large -> 28.dp
            FabSize.Normal -> 16.dp
            FabSize.Extended -> 16.dp
            else -> 12.dp
        }
    ),
    containerColor: Color = MaterialTheme.colorScheme.primaryContainer,
    contentColor: Color = contentColorFor(containerColor),
    elevation: FloatingActionButtonElevation = FloatingActionButtonDefaults.elevation(),
    delayMillis: Long = 0,
    durationMillis: Int = 500,
    contentDepth: Float = 1.1f,
    scaleAlignment: Alignment = Alignment.BottomEnd,
    visible: Boolean = isCompositionLaunched(delayMillis),
    content: @Composable RowScope.() -> Unit
) {
    Box(
        modifier = modifier.animateVisibility(
            visible,
            scale(
                0f,
                animationSpec = tween(durationMillis),
                transformOrigin = scaleAlignment.toTransformOrigin()
            )
        )
    ) {
        ActualFloatingActionButton(
            size,
            onClick,
            Modifier,
            interactionSource,
            shape,
            containerColor,
            contentColor,
            elevation
        ) {
            Row(
                modifier = Modifier.animateVisibility(
                    visible,
                    scale(
                        0f,
                        animationSpec = tween(
                            durationMillis,
                            delayMillis = (durationMillis * contentDepth).toInt() - durationMillis
                        ),
                        transformOrigin = scaleAlignment.toTransformOrigin()
                    )
                ),
                verticalAlignment = Alignment.CenterVertically,
                horizontalArrangement = Arrangement.spacedBy(12.dp)
            ) {
                content()
            }
        }
    }
}