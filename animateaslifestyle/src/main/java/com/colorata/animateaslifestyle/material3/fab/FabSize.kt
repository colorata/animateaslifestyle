package com.colorata.animateaslifestyle.material3.fab

enum class FabSize {
    Extended,
    Large,
    Normal,
    Small
}