package com.colorata.animateaslifestyle.material3.indicator

import androidx.compose.animation.core.*
import androidx.compose.foundation.Canvas
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.size
import androidx.compose.material3.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.geometry.Size
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.Path
import androidx.compose.ui.graphics.StrokeCap
import androidx.compose.ui.graphics.drawscope.Stroke
import androidx.compose.ui.graphics.drawscope.translate
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import com.colorata.animateaslifestyle.shapes.*
import kotlin.math.abs

@Composable
fun GenericCircularProgressIndicator(
    progress: Float,
    modifier: Modifier = Modifier,
    color: Color = MaterialTheme.colorScheme.primary,
    width: Dp = 1.dp,
    path: (Size) -> Path
) {
    Box(modifier = modifier.size(60.dp)) {
        Canvas(
            modifier = Modifier
                .fillMaxSize()
                .drawOffscreen()
        ) {
            val strokeWidth = width.toPx()
            translate(strokeWidth / 2, strokeWidth / 2) {
                drawPathWithArc(
                    path(
                        size.copy(size.width - strokeWidth, size.height - strokeWidth)
                    ),
                    arc = progressArc(progress),
                    color = color,
                    style = Stroke(width.toPx(), cap = StrokeCap.Round)
                )
            }
        }
    }
}

@Composable
fun GenericCircularProgressIndicator(
    modifier: Modifier = Modifier,
    color: Color = MaterialTheme.colorScheme.primary,
    width: Dp = 1.dp,
    spec: CircularIndeterminateSpec = CircularIndeterminateSpec(),
    path: (Size) -> Path
) {
    val arc = indeterminateArc(spec)
    Box(modifier = modifier.size(60.dp)) {
        Canvas(
            modifier = Modifier
                .fillMaxSize()
                .drawOffscreen()
        ) {
            drawPathWithArc(
                path(size),
                arc = arc,
                size = size,
                color = color, style = Stroke(width.toPx(), cap = StrokeCap.Round)
            )
        }
    }
}

@Composable
fun indeterminateArc(spec: CircularIndeterminateSpec = CircularIndeterminateSpec()): Arc {
    val transition = rememberInfiniteTransition()
    val currentRotation = transition.animateValue(
        0, spec.rotationsPerCycle, Int.VectorConverter, infiniteRepeatable(
            animation = tween(
                durationMillis = spec.rotationDuration * spec.rotationsPerCycle,
                easing = LinearEasing
            )
        )
    )
    val baseRotation = transition.animateFloat(
        0f, spec.baseRotationAngle, infiniteRepeatable(
            animation = tween(
                durationMillis = spec.rotationDuration, easing = LinearEasing
            )
        )
    )
    val endAngle = transition.animateFloat(
        0f,
        spec.jumpRotationAngle,
        infiniteRepeatable(animation = keyframes {
            durationMillis = spec.headAndTailAnimationDuration + spec.headAndTailDelayDuration
            0f at 0 with spec.easing
            spec.jumpRotationAngle at spec.headAndTailAnimationDuration
        })
    )
    val startAngle = transition.animateFloat(
        0f,
        spec.jumpRotationAngle,
        infiniteRepeatable(animation = keyframes {
            durationMillis = spec.headAndTailAnimationDuration + spec.headAndTailDelayDuration
            0f at spec.headAndTailDelayDuration with spec.easing
            spec.jumpRotationAngle at durationMillis
        })
    )

    val currentRotationAngleOffset =
        remember(currentRotation.value, spec.rotationAngleOffset) {
            (currentRotation.value * spec.rotationAngleOffset) % 360f
        }

    val sweep =
        remember(endAngle.value, startAngle.value) {
            abs(endAngle.value - startAngle.value)
        }

    val offset =
        remember(
            spec.startAngleOffset,
            currentRotationAngleOffset,
            baseRotation.value
        ) {
            spec.startAngleOffset + currentRotationAngleOffset + baseRotation.value
        }

    return arc(
        degrees(startAngle.value + offset), degrees(sweep)
    )
}


data class CircularIndeterminateSpec(
    val rotationsPerCycle: Int = 5,
    val rotationDuration: Int = 1332,
    val startAngleOffset: Float = -90f,
    val baseRotationAngle: Float = 286f,
    val jumpRotationAngle: Float = 290f,
    val rotationAngleOffset: Float = (baseRotationAngle + jumpRotationAngle) % 360f,
    val headAndTailAnimationDuration: Int = (rotationDuration * 0.5f).toInt(),
    val headAndTailDelayDuration: Int = headAndTailAnimationDuration,
    val easing: Easing = CubicBezierEasing(0.4f, 0f, 0.2f, 1f)
)