package com.colorata.animateaslifestyle.material3.indicator

import androidx.compose.material3.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import com.colorata.animateaslifestyle.material3.shapes.*
import com.colorata.animateaslifestyle.shapes.ExperimentalShapeApi

@ExperimentalShapeApi
@Composable
fun ScallopProgressIndicator(
    progress: Float,
    modifier: Modifier = Modifier,
    color: Color = MaterialTheme.colorScheme.primary,
    width: Dp = 1.dp
) {
    GenericCircularProgressIndicator(progress = progress, modifier, color, width) {
        MaterialPaths.scallopPath(it)
    }
}

@ExperimentalShapeApi
@Composable
fun ScallopProgressIndicator(
    modifier: Modifier = Modifier,
    color: Color = MaterialTheme.colorScheme.primary,
    width: Dp = 1.dp
) {
    GenericCircularProgressIndicator(modifier, color, width) {
        MaterialPaths.scallopPath(it)
    }
}
