package com.colorata.animateaslifestyle.material3.shapes

import androidx.compose.animation.core.TweenSpec
import androidx.compose.animation.core.animateFloatAsState
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.ui.geometry.Size
import androidx.compose.ui.graphics.Outline
import androidx.compose.ui.unit.Density
import androidx.compose.ui.unit.LayoutDirection
import com.colorata.animateaslifestyle.isCompositionLaunched
import com.colorata.animateaslifestyle.shapes.*

@ExperimentalShapeApi
class RoundedCornerShape(
    private val topStart: Float = 0.1f,
    private val topEnd: Float = 0.1f,
    private val bottomEnd: Float = 0.1f,
    private val bottomStart: Float = 0.1f,
    private val rotation: Angle = degrees(0f)
) : AnimatedShape {
    @Composable
    override fun animateToCircle(animationSpec: TweenSpec<Float>, rotation: Angle): AnimatedShape {
        val topStart by animateFloatAsState(
            targetValue = if (isCompositionLaunched()) 0.5f else this.topStart,
            animationSpec = animationSpec
        )
        val topEnd by animateFloatAsState(
            targetValue = if (isCompositionLaunched()) 0.5f else this.topEnd,
            animationSpec = animationSpec
        )
        val bottomStart by animateFloatAsState(
            targetValue = if (isCompositionLaunched()) 0.5f else this.bottomStart,
            animationSpec = animationSpec
        )
        val bottomEnd by animateFloatAsState(
            targetValue = if (isCompositionLaunched()) 0.5f else this.bottomEnd,
            animationSpec = animationSpec
        )
        return RoundedCornerShape(
            topStart, topEnd, bottomEnd, bottomStart, rotation
        )
    }

    @Composable
    override fun animateFromCircle(
        animationSpec: TweenSpec<Float>, rotation: Angle
    ): AnimatedShape {
        val topStart by animateFloatAsState(
            targetValue = if (!isCompositionLaunched()) 0.5f else this.topStart,
            animationSpec = animationSpec
        )
        val topEnd by animateFloatAsState(
            targetValue = if (!isCompositionLaunched()) 0.5f else this.topEnd,
            animationSpec = animationSpec
        )
        val bottomStart by animateFloatAsState(
            targetValue = if (!isCompositionLaunched()) 0.5f else this.bottomStart,
            animationSpec = animationSpec
        )
        val bottomEnd by animateFloatAsState(
            targetValue = if (!isCompositionLaunched()) 0.5f else this.bottomEnd,
            animationSpec = animationSpec
        )
        return RoundedCornerShape(
            topStart, topEnd, bottomEnd, bottomStart, rotation
        )
    }

    override fun createOutline(
        size: Size, layoutDirection: LayoutDirection, density: Density
    ): Outline {
        val min = minOf(size.width, size.height)
        return Outline.Generic(
            rectanglePath(
                size, topStart * min, topEnd * min, bottomStart * min, bottomEnd * min, rotation
            )
        )
    }
}