package com.colorata.animateaslifestyle.shapes

import androidx.compose.animation.core.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.Stable

/**
 * Angle class to keep angles consistent and not confusing
 * @param raw value of angle. Should not be used directly
 */
@Stable
sealed class Angle(val raw: Float) {
    /**
     * Angle in radians.
     * @param value value of angle in degrees
     */
    class Radians(val value: Float) : Angle(value)

    /**
     * Angle in degrees.
     * @param value value of angle in degrees
     */
    class Degrees(val value: Float) : Angle(value)
    companion object
}

operator fun Angle.plus(other: Angle): Angle {
    return degrees(this.degrees + other.degrees)
}

operator fun Angle.unaryMinus(): Angle {
    return degrees(-this.degrees)
}

operator fun Angle.div(other: Int): Angle {
    return degrees(this.degrees / other.toFloat())
}

/**
 * Converts current angle to degrees. Can be used only for radians
 */
fun Angle.Radians.toDegrees() = Angle.Degrees(value * 180f / pi)

/**
 * Converts current angle to radians. Can be used only for degrees
 */
fun Angle.Degrees.toRadians() = Angle.Radians(value * pi / 180f)

/**
 * Auto converted angle in radians
 */
val Angle.radians: Float
    get() {
        return when (this) {
            is Angle.Radians -> this.value
            is Angle.Degrees -> this.toRadians().value
        }
    }

/**
 * Auto converted angle in degrees
 */
val Angle.degrees: Float
    get() {
        return when (this) {
            is Angle.Radians -> this.toDegrees().value
            is Angle.Degrees -> this.value
        }
    }

fun degrees(value: Float) = Angle.Degrees(value)

fun radians(value: Float) = Angle.Radians(value)

@Suppress("UNCHECKED_CAST")
@Composable
fun animateAngleAsState(
    targetValue: Angle,
    animationSpec: AnimationSpec<Angle> = spring(),
    visibilityThreshold: Angle? = null,
    finishedListener: ((Angle) -> Unit)? = null
) =
    animateValueAsState(
        targetValue = targetValue,
        typeConverter = Angle.VectorConverter(targetValue),
        animationSpec, visibilityThreshold, finishedListener
    )

fun Angle.Companion.VectorConverter(
    targetValue: Angle
) = TwoWayConverter<Angle, AnimationVector1D>(
    convertToVector = {
        AnimationVector(it.raw)
    },
    convertFromVector = {
        when (targetValue) {
            is Angle.Degrees -> degrees(it.value)
            else -> radians(it.value)
        }
    }
)

@Composable
fun InfiniteTransition.animateAngle(
    initialValue: Angle,
    targetValue: Angle,
    animationSpec: InfiniteRepeatableSpec<Angle> = infiniteRepeatable(tween())
) = animateValue(
    initialValue = initialValue,
    targetValue = targetValue,
    typeConverter = Angle.VectorConverter(initialValue),
    animationSpec = animationSpec
)

@Composable
inline fun <S> Transition<S>.animateAngle(
    noinline transitionSpec:
    @Composable Transition.Segment<S>.() -> FiniteAnimationSpec<Angle> = { spring() },
    label: String = "FloatAnimation",
    targetValueByState: @Composable (state: S) -> Angle
) = animateValue(
    transitionSpec = transitionSpec,
    typeConverter = Angle.VectorConverter(
        targetValueByState(
            targetState
        )
    ), label = label,
    targetValueByState = targetValueByState
)

fun Animatable(initialValue: Angle, visibilityThreshold: Angle? = null) = Animatable(
    initialValue, Angle.VectorConverter(initialValue), visibilityThreshold
)