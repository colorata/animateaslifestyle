package com.colorata.animateaslifestyle.shapes

import androidx.compose.animation.core.*
import androidx.compose.runtime.*
import androidx.compose.ui.graphics.Shape
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch


val EaseOutSine: Easing = CubicBezierEasing(0.61f, 1f, 0.88f, 1f)
val EaseInSine: Easing = CubicBezierEasing(0.12f, 0f, 0.39f, 0f)

/**
 * Animated shape interface that lets you animate your own shapes.
 * @see animateShape
 */
@ExperimentalShapeApi
interface AnimatedShape : Shape {
    /**
     * Function to animate shape to circle. It will be animated as soon as this function called(except delay options)
     *
     * ***NOTE:***
     *
     * To smoothly animate between shapes, circle should have same width and height and fit to box.
     * @param animationSpec animation spec for this function
     * @param rotation rotation of this shape. You need to animate outside of this function
     */
    @Composable
    fun animateToCircle(
        animationSpec: TweenSpec<Float>,
        rotation: Angle
    ): AnimatedShape

    /**
     * Function to animate circle to shape. It will be animated as soon as this function called(except delay options)
     *
     * ***NOTE:***
     *
     * To smoothly animate between shapes, circle should have same width and height and fit to box.
     * @param animationSpec animation spec for this function
     * @param rotation rotation of this shape. You need to animate outside of this function
     */
    @Composable
    fun animateFromCircle(
        animationSpec: TweenSpec<Float>,
        rotation: Angle
    ): AnimatedShape
}

/**
 * This function lets you animate given shape when it changes.
 *
 * ***NOTE:***
 * You need to wrap target value in **remember** braces and update it only when needed. It's needed because [AnimatedShape] is not stable(in Compose terms)
 * @param targetValue shape. When it changes, animation starts
 * @param animationSpec animation spec for animation. Keep in mind that easing will be ignored because of smooth
 * @param rotation rotation in animation in degrees
 * @return animated shape
 * @sample com.colorata.animateaslifestyle.samples.AnimateShapeSample()
 */
@ExperimentalShapeApi
@Composable
fun animateShape(
    targetValue: AnimatedShape,
    animationSpec: TweenSpec<Float> = tween(),
    rotation: Angle = degrees(90f)
): AnimatedShape {
    var isLaunched by remember { mutableStateOf(false) }
    var finalTarget by remember { mutableStateOf(targetValue) }
    var state by remember { mutableStateOf(ShapeState.Static) }
    val rotationToCircle = remember { Animatable(degrees(0f)) }
    val rotationFromCircle = remember { Animatable(-rotation / 2) }
    LaunchedEffect(targetValue) {
        if (!isLaunched) isLaunched = true
        else {
            delay(animationSpec.delay.toLong())
            launch {
                rotationToCircle.animateTo(
                    rotation / 2,
                    tween(animationSpec.durationMillis / 2, easing = LinearOutSlowInEasing)
                )
            }
            state = ShapeState.ToCircle
            delay(animationSpec.durationMillis.toLong() / 2)
            launch {
                rotationFromCircle.animateTo(
                    degrees(0f),
                    tween(animationSpec.durationMillis / 2, easing = EaseOutSine)
                )
            }
            finalTarget = targetValue
            state = ShapeState.FromCircle
            delay(animationSpec.durationMillis.toLong() / 2)
            state = ShapeState.Static
            rotationToCircle.snapTo(degrees(0f))
            rotationFromCircle.snapTo(degrees(0f))
        }
    }

    return finalTarget.let {
        when (state) {
            ShapeState.Static -> it
            ShapeState.FromCircle -> {
                it.animateFromCircle(
                    animationSpec = tween(
                        animationSpec.durationMillis / 2,
                        0,
                        EaseOutSine
                    ),
                    rotationFromCircle.value
                )
            }
            else -> {
                it.animateToCircle(
                    animationSpec = tween(
                        animationSpec.durationMillis / 2,
                        0,
                        EaseInSine
                    ),
                    rotationToCircle.value
                )
            }
        }
    }
}

enum class ShapeState {
    Static,
    FromCircle,
    ToCircle
}