package com.colorata.animateaslifestyle.shapes

import android.graphics.Canvas
import android.graphics.RectF
import android.os.Build
import androidx.annotation.FloatRange
import androidx.annotation.RequiresApi
import androidx.compose.animation.core.*
import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.Canvas
import androidx.compose.foundation.layout.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.Stable
import androidx.compose.runtime.State
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.draw.drawWithContent
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.geometry.Size
import androidx.compose.ui.graphics.*
import androidx.compose.ui.graphics.drawscope.DrawScope
import androidx.compose.ui.graphics.drawscope.DrawStyle
import androidx.compose.ui.graphics.drawscope.Fill
import androidx.compose.ui.graphics.drawscope.Stroke
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.platform.LocalLayoutDirection
import com.colorata.animateaslifestyle.material3.indicator.indeterminateArc
import kotlin.math.pow
import kotlin.math.sqrt


/**
 * Data class to keep arc parameters.
 * All values are used in polar system
 * @param startAngle Start angle of arc
 * @param sweepAngle Sweep angle of arc
 * @param useCenter Is center will be used
 */
@Stable
data class Arc(
    val startAngle: Angle, val sweepAngle: Angle, val useCenter: Boolean = true
) {
    companion object
}

/**
 * Full arc(circle)
 *
 * *start* - 0 in degrees
 *
 * *sweep* - 360f in degrees
 */
val Arc.Companion.Full: Arc
    get() = arc(degrees(0f), degrees(360f))

/**
 * Creates progress arc with given [progress]
 * @param progress progress from 0 to 100
 */
fun progressArc(@FloatRange(from = 0.0, to = 100.0) progress: Float) =
    arc(degrees(-90f), degrees(progress / 100f * 360f))

/**
 * Creates arc with given [startAngle] and [sweepAngle]
 *
 * ***NOTE:***
 *
 * When [startAngle] increases, it moves clockwise. 0 degrees equals to 3 o'clock
 * @param startAngle Start angle
 * @param sweepAngle Sweep angle
 * @param useCenter Is arc will use center
 */
fun arc(startAngle: Angle, sweepAngle: Angle, useCenter: Boolean = true) =
    Arc(startAngle, sweepAngle, useCenter)

/**
 * Creates mask for given [arc]
 * @param arc Arc that will be used as a mask
 */
fun Modifier.withArc(
    arc: Arc
) = drawWithContent {
    with(drawContext.canvas.nativeCanvas) {
        val checkPoint = saveLayer(null, null)
        drawContent()
        this@drawWithContent.drawMaskArc(arc, color = Color.Black)
        restoreToCount(checkPoint)
    }
}

/**
 * Creates border for given [arc] with given [shape]
 * @param arc Arc that will affect border
 * @param border Border stroke
 * @param shape Shape of border
 * @param content Composition that will be affected
 */
@Composable
fun ArcBorder(
    arc: Arc, border: BorderStroke, shape: Shape = RectangleShape, content: @Composable () -> Unit
) {
    val layoutDirection = LocalLayoutDirection.current
    val density = LocalDensity.current
    Box(
        modifier = Modifier
            .clip(shape)
            .height(IntrinsicSize.Min)
            .width(IntrinsicSize.Min)
    ) {
        content()
        Canvas(
            modifier = Modifier
                .fillMaxSize()
                .drawOffscreen()
        ) {
            drawOutline(
                shape.createOutline(size, layoutDirection, density),
                brush = border.brush,
                style = Stroke(border.width.toPx(), cap = StrokeCap.Round)
            )
            drawMaskArc(arc, border.brush)
        }
    }
}


/**
 * Creates border for given [progress] with given [shape]
 * @param progress Progress that will affect border from 0 to 100
 * @param border Border stroke
 * @param shape Shape of border
 * @param content Composition that will be affected
 */
@Composable
fun ArcBorder(
    @FloatRange(from = 0.0, to = 100.0) progress: Float,
    border: BorderStroke,
    shape: Shape = RectangleShape,
    content: @Composable () -> Unit
) = ArcBorder(progressArc(progress), border, shape, content)

/**
 * Creates indeterminate border with given [shape]
 * @param border Border stroke
 * @param shape Shape of border
 * @param content Composition that will be affected
 */
@Composable
fun ArcBorder(
    border: BorderStroke, shape: Shape = RectangleShape, content: @Composable () -> Unit
) = ArcBorder(indeterminateArc(), border, shape, content)

/**
 * Animates given arc as state
 * @param targetValue Target value of the animation
 * @param animationSpec The animation that will be used to change the value through time. Physics animation will be used by default.
 * @param visibilityThreshold An optional threshold to define when the animation value can be considered close enough to the targetValue to end the animation.
 * @param finishedListener An optional end listener to get notified when the animation is finished.
 * @return A [State] object, the value of which is updated by animation.
 */
@Composable
fun animateArcAsState(
    targetValue: Arc,
    animationSpec: AnimationSpec<Arc> = spring(),
    visibilityThreshold: Arc? = null,
    finishedListener: ((Arc) -> Unit)? = null
) = animateValueAsState(
    targetValue = targetValue,
    typeConverter = Arc.VectorConverter(targetValue),
    animationSpec,
    visibilityThreshold,
    finishedListener
)

/**
 * [TwoWayConverter] for arc
 * @param targetValue Target value of animation
 */
fun Arc.Companion.VectorConverter(targetValue: Arc) = TwoWayConverter<Arc, AnimationVector2D>(convertToVector = {
    AnimationVector(it.startAngle.raw, it.sweepAngle.raw)
}, convertFromVector = {
    val finalStartAngle =
        if (targetValue.startAngle is Angle.Degrees) degrees(it.v1) else radians(it.v1)
    val finalSweepAngle =
        if (targetValue.sweepAngle is Angle.Degrees) degrees(it.v2) else radians(it.v2)

    arc(
        startAngle = finalStartAngle, sweepAngle = finalSweepAngle, targetValue.useCenter
    )
})

/**
 * Infinitely animates arc with [InfiniteTransition]
 * @param initialValue Initial arc that will be animated
 * @param targetValue Finish arc - end point
 * @param animationSpec Animation spec
 */
@Composable
fun InfiniteTransition.animateArc(
    initialValue: Arc,
    targetValue: Arc,
    animationSpec: InfiniteRepeatableSpec<Arc> = infiniteRepeatable(tween())
) = animateValue(
    initialValue = initialValue,
    targetValue = targetValue,
    typeConverter = Arc.VectorConverter(initialValue),
    animationSpec = animationSpec
)

/**
 * Animates arc with given [Transition]
 * @param transitionSpec Animation spec
 * @param label Label of animation. Can be used for debugging in Android Studio
 * @param targetValueByState Function that will return target value
 */
@Composable
inline fun <S> Transition<S>.animateArc(
    noinline transitionSpec: @Composable Transition.Segment<S>.() -> FiniteAnimationSpec<Arc> = { spring() },
    label: String = "FloatAnimation",
    targetValueByState: @Composable (state: S) -> Arc
) = animateValue(
    transitionSpec = transitionSpec, typeConverter = Arc.VectorConverter(
        targetValueByState(
            targetState
        )
    ), label = label, targetValueByState = targetValueByState
)

/**
 * Creates [Animatable] function that can be used in non-composable function
 * @param initialValue initial value of the animatable value holder
 * @param visibilityThreshold Threshold at which the animation may round off to its target value.
 */
fun Animatable(initialValue: Arc, visibilityThreshold: Arc? = null) = Animatable(
    initialValue, Arc.VectorConverter(initialValue), visibilityThreshold
)

/**
 * Function to draw standard arc with [Arc] in [DrawScope]
 *
 * ***NOTE:***
 *
 * Please apply [drawOffscreen] modifier to get expected result
 * @param color Color to be applied to the arc
 * @param arc Arc of circle
 * @param topLeft Offset from the local origin of 0, 0 relative to the current translation
 * @param size Dimensions of the arc to draw
 * @param alpha Opacity to be applied to the arc from 0.0f to 1.0f representing
 * fully transparent to fully opaque respectively
 * @param style Whether or not the arc is stroked or filled in
 * @param colorFilter ColorFilter to apply to the [color] when drawn into the destination
 * @param blendMode Blending algorithm to be applied to the arc when it is drawn
 */
fun DrawScope.drawArc(
    color: Color,
    arc: Arc,
    topLeft: Offset = Offset.Zero,
    size: Size = this.size.offsetSize(topLeft),
    alpha: Float = 1.0f,
    style: DrawStyle = Fill,
    colorFilter: ColorFilter? = null,
    blendMode: BlendMode = DrawScope.DefaultBlendMode
) {
    drawArc(
        color = color,
        startAngle = arc.startAngle.degrees,
        sweepAngle = arc.sweepAngle.degrees,
        useCenter = arc.useCenter,
        topLeft,
        size,
        alpha,
        style,
        colorFilter,
        blendMode
    )
}

fun DrawScope.drawArc(
    brush: Brush,
    arc: Arc,
    topLeft: Offset = Offset.Zero,
    size: Size = this.size.offsetSize(topLeft),
    alpha: Float = 1.0f,
    style: DrawStyle = Fill,
    colorFilter: ColorFilter? = null,
    blendMode: BlendMode = DrawScope.DefaultBlendMode
) {
    drawArc(
        brush = brush,
        startAngle = arc.startAngle.degrees,
        sweepAngle = arc.sweepAngle.degrees,
        useCenter = arc.useCenter,
        topLeft,
        size,
        alpha,
        style,
        colorFilter,
        blendMode
    )
}

/**
 * Function to draw standard arc with [Arc] in native [Canvas]
 *
 * ***NOTE:***
 *
 * Please apply [drawOffscreen] modifier to get expected result
 * @param color Color to be applied to the arc
 * @param arc Arc of circle
 * @param topLeft Offset from the local origin of 0, 0 relative to the current translation
 * @param size Dimensions of the arc to draw
 * @param alpha Opacity to be applied to the arc from 0.0f to 1.0f representing
 * fully transparent to fully opaque respectively
 * @param style Whether or not the arc is stroked or filled in
 * @param colorFilter ColorFilter to apply to the [color] when drawn into the destination
 * @param blendMode Blending algorithm to be applied to the arc when it is drawn
 */
fun Canvas.drawArc(
    color: Color,
    arc: Arc,
    topLeft: Offset = Offset.Zero,
    size: Size = Size(width.toFloat(), height.toFloat()),
    alpha: Float = 1.0f,
    style: android.graphics.Paint.Style = android.graphics.Paint.Style.FILL,
    colorFilter: ColorFilter? = null,
    blendMode: android.graphics.BlendMode? = null
) {
    drawArc(RectF(
        topLeft.x, topLeft.y, size.width + topLeft.x, size.height + topLeft.y
    ),
        arc.startAngle.degrees,
        arc.sweepAngle.degrees,
        arc.useCenter,
        android.graphics.Paint().apply {
            this.alpha = (alpha * 100).toInt()
            if (colorFilter != null) {
                this.colorFilter = colorFilter.asAndroidColorFilter()
            }
            this.color = color.toArgb()
            this.style = style
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q && blendMode != null) {
                this.blendMode = blendMode
            }
        })
}

/**
 * Function to draw any path with given arc in [DrawScope]
 *
 * ***NOTE:***
 *
 * Please apply [drawOffscreen] modifier to get expected result
 * @param path original path
 * @param color Color of path
 * @param arc arc of path that will be visible
 * @param topLeft Top left offset of path in absolute coordinates
 * @param size Size of path
 * @param alpha Alpha of path
 * @param style Style of path
 * @param colorFilter Color filter for path
 */
fun DrawScope.drawPathWithArc(
    path: Path,
    color: Color,
    arc: Arc,
    topLeft: Offset = Offset.Zero,
    size: Size = this.size.offsetSize(topLeft),
    alpha: Float = 1.0f,
    style: DrawStyle = Fill,
    colorFilter: ColorFilter? = null
) {
    drawPath(
        path, color, alpha, style, colorFilter
    )
    drawMaskArc(arc, color, topLeft, size)
}

/**
 * Function to draw any path with given arc for native [Canvas]
 *
 * ***NOTE:***
 *
 * Please apply [drawOffscreen] modifier to get expected result
 * @param path original path
 * @param color Color of path
 * @param arc arc of path that will be visible
 * @param topLeft Top left offset of path in absolute coordinates
 * @param size Size of path
 * @param alpha Alpha of path
 * @param style Style of path
 * @param colorFilter Color filter for path
 */
@RequiresApi(Build.VERSION_CODES.Q)
fun Canvas.drawPathWithArc(
    path: Path,
    color: Color,
    arc: Arc,
    topLeft: Offset = Offset.Zero,
    size: Size = Size(width.toFloat(), height.toFloat()),
    alpha: Float = 1.0f,
    style: android.graphics.Paint.Style = android.graphics.Paint.Style.FILL,
    colorFilter: ColorFilter? = null
) {
    drawPath(path.asAndroidPath(), android.graphics.Paint().apply {
        this.color = color.toArgb()
        this.alpha = (alpha * 100).toInt()
        this.style = style
        if (colorFilter != null) {
            this.colorFilter = colorFilter.asAndroidColorFilter()
        }
    })
    drawMaskArc(arc, color, topLeft, size)
}

fun DrawScope.drawMaskArc(
    arc: Arc,
    color: Color,
    topLeft: Offset = Offset.Zero,
    size: Size = this.size.offsetSize(topLeft),
    blendMode: BlendMode = BlendMode.DstOut
) {
    val radius = sqrt(size.width.pow(2) + size.height.pow(2)) / 2
    drawArc(
        color, arc.copy(
            startAngle = degrees(arc.sweepAngle.degrees + arc.startAngle.degrees),
            sweepAngle = degrees(360f - arc.sweepAngle.degrees)
        ), topLeft = Offset(
            topLeft.x + size.width / 2 - radius, topLeft.y + size.height / 2 - radius
        ), size = Size(
            radius * 2, radius * 2
        ), blendMode = blendMode
    )
}

fun DrawScope.drawMaskArc(
    arc: Arc,
    brush: Brush,
    topLeft: Offset = Offset.Zero,
    size: Size = this.size.offsetSize(topLeft),
    blendMode: BlendMode = BlendMode.DstOut
) {
    val radius = sqrt(size.width.pow(2) + size.height.pow(2)) / 2
    drawArc(
        brush, arc.copy(
            startAngle = degrees(arc.sweepAngle.degrees + arc.startAngle.degrees),
            sweepAngle = degrees(360f - arc.sweepAngle.degrees)
        ), topLeft = Offset(
            topLeft.x + size.width / 2 - radius, topLeft.y + size.height / 2 - radius
        ), size = Size(
            radius * 2, radius * 2
        ), blendMode = blendMode
    )
}

@RequiresApi(Build.VERSION_CODES.Q)
fun Canvas.drawMaskArc(
    arc: Arc,
    color: Color,
    topLeft: Offset = Offset.Zero,
    size: Size = Size(width.toFloat(), height.toFloat()).offsetSize(topLeft),
    blendMode: android.graphics.BlendMode = android.graphics.BlendMode.DST_OUT
) {
    val radius = sqrt(size.width.pow(2) + size.height.pow(2)) / 2
    drawArc(
        color, arc.copy(
            startAngle = degrees(arc.sweepAngle.degrees + arc.startAngle.degrees),
            sweepAngle = degrees(360f - arc.sweepAngle.degrees)
        ), topLeft = Offset(
            topLeft.x + size.width / 2 - radius, topLeft.y + size.height / 2 - radius
        ), size = Size(
            radius * 2, radius * 2
        ), blendMode = blendMode
    )
}

fun Size.offsetSize(offset: Offset): Size = Size(this.width - offset.x, this.height - offset.y)
