@file:Suppress("unused")

package com.colorata.animateaslifestyle.shapes

import kotlin.math.*

/**
 * Class to create Coordinate in one System
 * Available systems:
 * - Rectangular
 * - Polar
 */
sealed class CoordinateSystem {
    /**
     * Creates rectangular coordinate in Rectangular system
     * @param x X coordinate(horizontal axis)
     * @param y Y coordinate(vertical axis)
     */
    data class Rectangular(
        val x: Float, val y: Float
    ) : CoordinateSystem()

    /**
     * Creates polar coordinate in Polar System
     * @param radius Radius of coordinate
     * @param theta Theta(angle) of coordinate in radians
     */
    data class Polar(
        val radius: Float, val theta: Angle
    ) : CoordinateSystem()
}

/**
 * Creates polar coordinate in Polar System
 * @param radius Radius of coordinate
 * @param theta Theta(angle) of coordinate in radians
 */
fun polarCoordinates(radius: Float, theta: Angle) = CoordinateSystem.Polar(radius, theta)

/**
 * Creates rectangular coordinate in Rectangular system
 * @param x X coordinate(horizontal axis)
 * @param y Y coordinate(vertical axis)
 */
fun rectangularCoordinates(x: Float, y: Float) = CoordinateSystem.Rectangular(x, y)

/**
 * Converts [CoordinateSystem.Polar] to [CoordinateSystem.Rectangular]
 * @return [CoordinateSystem.Rectangular]
 */
fun CoordinateSystem.Polar.toRectangular() = rectangularCoordinates(
    x = radius * cos(theta.radians), y = radius * sin(theta.radians)
)

/**
 * Converts [CoordinateSystem.Rectangular] coordinates to [CoordinateSystem.Polar]
 */
fun CoordinateSystem.Rectangular.toPolar() =
    polarCoordinates(sqrt(x.pow(2) + y.pow(2)), radians(atan2(y, x)))

/**
 * Returns polar coordinates of [CoordinateSystem]
 */
val CoordinateSystem.polarCoordinates: CoordinateSystem.Polar
    get() {
        return when (this) {
            is CoordinateSystem.Polar -> this
            is CoordinateSystem.Rectangular -> toPolar()
        }
    }

/**
 * Returns rectangular coordinates of [CoordinateSystem]
 */
val CoordinateSystem.rectangularCoordinates: CoordinateSystem.Rectangular
    get() {
        return when (this) {
            is CoordinateSystem.Polar -> toRectangular()
            is CoordinateSystem.Rectangular -> this
        }
    }