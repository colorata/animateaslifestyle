package com.colorata.animateaslifestyle.shapes

@RequiresOptIn(
    "This shape API is experimental and is likely to change or to be removed in" +
            " the future.",
    level = RequiresOptIn.Level.WARNING
)
annotation class ExperimentalShapeApi
