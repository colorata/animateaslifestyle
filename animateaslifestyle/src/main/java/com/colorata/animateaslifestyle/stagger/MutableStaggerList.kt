package com.colorata.animateaslifestyle.stagger

import androidx.compose.animation.core.Animatable
import androidx.compose.runtime.snapshots.SnapshotStateList
import androidx.compose.runtime.toMutableStateList
import kotlinx.coroutines.delay

@ExperimentalStaggerApi
internal typealias MutableStaggerList<T, A> = SnapshotStateList<Element<T, A>>


/**
 * Creates [StaggerList] for given [values] with [animationValue] for each.
 * @param animationValue Initial value for [Animatable] function with given [T]
 * @param values List of values
 * @return [StaggerList]
 */
@ExperimentalStaggerApi
internal fun <T, A> mutableStaggerListOf(
    animationValue: (T) -> A, visible: Boolean = false, vararg values: T
): MutableStaggerList<T, A> {
    return values.toMutableStaggerList(animationValue, visible)
}

@ExperimentalStaggerApi
internal suspend fun <T, A> MutableStaggerList<T, A>.add(
    element: T,
    animationValue: (T) -> A,
    visible: Boolean = false,
    spec: StaggerSpec<T, A>
) = add(size, element, animationValue, visible, spec)

@ExperimentalStaggerApi
internal suspend fun <T, A> MutableStaggerList<T, A>.addAll(
    elements: Collection<T>,
    animationValue: (T) -> A,
    visible: Boolean = false,
    spec: StaggerSpec<T, A>
) = addAll(size, elements, animationValue, visible, spec)

@ExperimentalStaggerApi
internal suspend fun <T, A> MutableStaggerList<T, A>.add(
    index: Int,
    element: T,
    animationValue: (T) -> A,
    visible: Boolean = false,
    spec: StaggerSpec<T, A>
) {
    val e = Element(element, animationValue, visible)
    delay(spec.initialDelayMillis)
    add(index, e)
    spec.function(e)
}

@ExperimentalStaggerApi
internal suspend fun <T, A> MutableStaggerList<T, A>.addAll(
    index: Int,
    elements: Collection<T>,
    animationValue: (T) -> A,
    visible: Boolean = false,
    spec: StaggerSpec<T, A>
) {
    val e = elements.map { Element(it, animationValue, visible) }
    addAll(index, e)
    delay(spec.initialDelayMillis - spec.itemsDelayMillis)
    e.forEach {
        spec.itemsDelayMillis
        spec.function(it)
    }
}

@ExperimentalStaggerApi
internal suspend fun <T, A> MutableStaggerList<T, A>.remove(
    element: T,
    spec: StaggerSpec<T, A>
): Boolean {
    val finder = find { it.value == element }
    delay(spec.initialDelayMillis)
    finder?.let { spec.function(it) }
    delay(spec.itemsDelayMillis)
    return remove(finder)
}

@ExperimentalStaggerApi
internal suspend fun <T, A> MutableStaggerList<T, A>.removeAll(
    elements: Collection<T>,
    spec: StaggerSpec<T, A>
): Boolean {
    val finder = filter { it.value in elements }
    delay(spec.initialDelayMillis - spec.itemsDelayMillis)
    finder.forEach {
        delay(spec.itemsDelayMillis)
        spec.function(it)
    }
    delay(spec.itemsDelayMillis)
    return removeAll(finder)
}

@ExperimentalStaggerApi
internal suspend fun <T, A> MutableStaggerList<T, A>.retainAll(
    elements: Collection<T>,
    spec: StaggerSpec<T, A>
): Boolean {
    val finder = filter { it.value !in elements }
    delay(spec.initialDelayMillis - spec.itemsDelayMillis)
    finder.forEach {
        delay(spec.itemsDelayMillis)
        spec.function(it)
    }
    delay(spec.itemsDelayMillis)
    return removeAll(finder)
}

@ExperimentalStaggerApi
internal suspend fun <T, A> MutableStaggerList<T, A>.removeAt(
    index: Int,
    spec: StaggerSpec<T, A>
): Boolean {
    val finder = this[index]
    delay(spec.initialDelayMillis)
    spec.function(finder)
    delay(spec.itemsDelayMillis)
    return remove(finder)
}

@ExperimentalStaggerApi
internal suspend fun <T, A> MutableStaggerList<T, A>.clear(
    spec: StaggerSpec<T, A>
) {
    delay(spec.initialDelayMillis - spec.itemsDelayMillis)
    forEach {
        delay(spec.itemsDelayMillis)
        spec.function(it)
    }
    delay(spec.itemsDelayMillis)
    return clear()
}

/**
 * Converts [Collection] to [StaggerList] with given [animationValue]
 * @param animationValue Initial value for [Animatable] function
 * @return [StaggerList]
 */
@ExperimentalStaggerApi
internal fun <T, A> Collection<T>.toMutableStaggerList(
    animationValue: (T) -> A, visible: Boolean = false
) = toStaggerList(animationValue, visible).toMutableStateList()

/**
 * Converts [Array] to [StaggerList] with given [animationValue]
 * @param animationValue Initial value for [Animatable] function
 * @return [StaggerList]
 */
@ExperimentalStaggerApi
internal fun <T, A> Array<out T>.toMutableStaggerList(animationValue: (T) -> A, visible: Boolean = false) =
    toList().toMutableStaggerList(animationValue, visible)
