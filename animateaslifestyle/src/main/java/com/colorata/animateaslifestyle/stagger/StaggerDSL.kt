@file:Suppress("unused")

package com.colorata.animateaslifestyle.stagger

import androidx.compose.animation.core.*
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.lazy.*
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.unit.Dp
import com.colorata.animateaslifestyle.Transition
import com.colorata.animateaslifestyle.animateVisibility
import com.colorata.animateaslifestyle.fade
import kotlinx.collections.immutable.ImmutableList
import kotlinx.collections.immutable.PersistentList
import kotlinx.collections.immutable.toImmutableList
import kotlinx.collections.immutable.toPersistentList
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking

class StaggerList<T, A> internal constructor(private val base: ImmutableList<Element<T, A>>) :
    ImmutableList<Element<T, A>> {
    override val size: Int
        get() = base.size

    override fun get(index: Int) = base[index]

    override fun isEmpty() = base.isEmpty()

    override fun iterator() = base.iterator()

    override fun listIterator() = base.listIterator()

    override fun listIterator(index: Int) = base.listIterator(index)

    override fun subList(fromIndex: Int, toIndex: Int) =
        base.subList(fromIndex, toIndex).toImmutableList()

    override fun lastIndexOf(element: Element<T, A>) = base.lastIndexOf(element)

    override fun indexOf(element: Element<T, A>) = base.indexOf(element)

    override fun containsAll(elements: Collection<Element<T, A>>) = base.containsAll(elements)

    override fun contains(element: Element<T, A>) = base.contains(element)

}

data class StaggerSpec<T, A>(
    val initialDelayMillis: Long = 0,
    val itemsDelayMillis: Long = 50,
    val function: suspend Element<T, A>.() -> Unit
)

fun <T, A> staggerSpecOf(
    initialDelayMillis: Long = 0,
    itemsDelayMillis: Long = 50,
    function: suspend Element<T, A>.() -> Unit
) = StaggerSpec(initialDelayMillis, itemsDelayMillis, function)

fun <T, A> elementOf(value: T, animationValue: (T) -> A, visible: Boolean = false) =
    Element(value, animationValue, visible)

/**
 * Class with one animation value.
 * @param initialValue Value for some type of information
 * @param initialAnimValue Initial value for [Animatable] function
 * @param initialVisible Is element initially visible
 * @see StaggerList
 */
data class Element<T, A>(
    private val initialValue: T,
    private val initialAnimValue: (T) -> A,
    private val initialVisible: Boolean = false
) {
    var value by mutableStateOf(initialValue)
    val animationValue = initialAnimValue(initialValue)
    var visible: Boolean by mutableStateOf(initialVisible)
}

/**
 * Creates [StaggerList] for given [values] with [animationValue] for each.
 * @param animationValue Initial value for [Animatable] function with given [T]
 * @param values List of values
 * @return [StaggerList]
 */
@ExperimentalStaggerApi
fun <T, A> staggerListOf(
    animationValue: (T) -> A, visible: Boolean = false, vararg values: T
): StaggerList<T, A> {
    return values.toStaggerList(animationValue, visible)
}

/**
 * Converts [Collection] to [StaggerList] with given [animationValue]
 * @param animationValue Initial value for [Animatable] function
 * @return [StaggerList]
 */
@ExperimentalStaggerApi
fun <T, A> Collection<T>.toStaggerList(
    animationValue: (T) -> A, visible: Boolean = false
): StaggerList<T, A> {
    return StaggerList(mapIndexed { _, it ->
        Element(
            it,
            animationValue,
            visible
        )
    }.toPersistentList())
}

/**
 * Converts [Array] to [StaggerList] with given [animationValue]
 * @param animationValue Initial value for [Animatable] function
 * @return [StaggerList]
 */
@ExperimentalStaggerApi
fun <T, A> Array<out T>.toStaggerList(animationValue: (T) -> A, visible: Boolean = false) =
    toList().toStaggerList(animationValue, visible)

/**
 * Animates all **list** with with given [scope] with *Stagger* effect
 * @param scope [CoroutineScope] of this function
 * @param startIndex Index of element where list starts animating
 */
suspend fun <T, A> StaggerList<T, A>.animateAsList(
    scope: CoroutineScope,
    startIndex: Int = 0,
    spec: StaggerSpec<T, A>
) {
    runBlocking {
        fun animate(index: Int, forward: Boolean = true) {
            if (index in indices) {
                scope.launch {
                    with(spec) {
                        function(get(index))
                    }
                }
                scope.launch {
                    delay(spec.itemsDelayMillis)
                    animate(if (forward) index + 1 else index - 1, forward)
                }
            }
        }

        delay(spec.initialDelayMillis)
        if (startIndex > 0) {
            scope.launch {
                animate(startIndex, false)
            }
            scope.launch {
                animate(startIndex)
            }
        } else {
            animate(startIndex)
        }
    }
}

/**
 * Animates all **grid** with with given [scope] with *Stagger* effect
 * @param scope [CoroutineScope] of this function
 * @param startIndex Index of element where list starts animating
 * @param cells Count of cells in grid
 */
suspend fun <T, A> StaggerList<T, A>.animateAsGrid(
    scope: CoroutineScope,
    startIndex: Int = 0,
    cells: Int,
    spec: StaggerSpec<T, A>
) {
    runBlocking {
        val already = this@animateAsGrid.map { false }.toMutableList()
        fun animate(index: Int, forward: Boolean = true) {
            if (index in indices && !already[index]) {
                scope.launch {
                    spec.function(get(index))
                }
                scope.launch {
                    val finalIndex = if (forward) index + 1 else index - 1
                    if (finalIndex in indices) {
                        delay(spec.itemsDelayMillis)
                        animate(finalIndex, forward)
                        already[finalIndex] = true
                    }
                }
                scope.launch {
                    val finalIndex = if (forward) index + cells else index - cells
                    if (finalIndex in indices) {
                        delay(spec.itemsDelayMillis)
                        animate(finalIndex, forward)
                        already[finalIndex] = true
                    }
                }
            }
        }

        delay(spec.initialDelayMillis)
        if (startIndex > 0) {
            scope.launch {
                animate(startIndex, false)
            }
            scope.launch {
                animate(startIndex)
            }
        } else {
            animate(startIndex)
        }
    }
}

inline fun <T, A> LazyListScope.staggerItems(
    items: List<Element<T, A>>,
    crossinline transition: (item: Element<T, A>) -> Transition = { fade() },
    noinline key: ((item: Element<T, A>) -> Any)? = null,
    noinline contentType: (item: Element<T, A>) -> Any? = { null },
    crossinline itemContent: @Composable LazyItemScope.(item: Element<T, A>) -> Unit
) {
    items(items, key, contentType) {
        Box(modifier = Modifier.animateVisibility(it.visible, transition(it))) {
            itemContent(it)
        }
    }
}

inline fun <T, A> LazyListScope.staggerItemsIndexed(
    items: List<Element<T, A>>,
    crossinline transition: (index: Int, item: Element<T, A>) -> Transition = { _, _ -> fade() },
    noinline key: ((index: Int, item: Element<T, A>) -> Any)? = null,
    crossinline contentType: (index: Int, item: Element<T, A>) -> Any? = { _, _ -> null },
    crossinline itemContent: @Composable LazyItemScope.(index: Int, item: Element<T, A>) -> Unit
) {
    itemsIndexed(items, key, contentType) { index, it ->
        Box(modifier = Modifier.animateVisibility(it.visible, transition(index, it))) {
            itemContent(index, it)
        }
    }
}

/**
 * Converts [Dp] to px
 */
@Composable
fun Dp.toPx() = with(LocalDensity.current) { toPx() }

/**
 * Converts px to [Dp]
 */
@Composable
fun Int.toDp() = with(LocalDensity.current) { toDp() }

/**
 * Converts px to [Dp]
 */
@Composable
fun Float.toDp() = with(LocalDensity.current) { toDp() }
