package com.colorata.animateaslifestyle.stagger

import kotlinx.collections.immutable.mutate
import kotlinx.collections.immutable.persistentListOf

fun <T, A> Collection<Element<T, A>>.asStaggerList(): StaggerList<T, A> {
    return StaggerList(persistentListOf<Element<T, A>>().mutate { list ->
        forEach {
            list.add(it)
        }
    })
}

fun <T, A> ArrayList<Element<T, A>>.asStaggerList(): StaggerList<T, A> {
    return StaggerList(persistentListOf<Element<T, A>>().mutate { list ->
        forEach {
            list.add(it)
        }
    })
}