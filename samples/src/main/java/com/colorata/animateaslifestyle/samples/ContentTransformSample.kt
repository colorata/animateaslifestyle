package com.colorata.animateaslifestyle.samples

import androidx.compose.animation.AnimatedContent
import androidx.compose.animation.ContentTransform
import androidx.compose.animation.ExperimentalAnimationApi
import androidx.compose.animation.SizeTransform
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.size
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.colorata.animateaslifestyle.fade
import com.colorata.animateaslifestyle.scale
import com.colorata.animateaslifestyle.slideVertically
import com.colorata.animateaslifestyle.with

@Preview
@OptIn(ExperimentalAnimationApi::class)
@Composable
fun ContentTransformSample() {
    AnimationSample<Float> { visible, spec ->
        AnimatedContent(
            visible,
            transitionSpec = {
                (fade() + slideVertically(-500f) with fade() + slideVertically(500f)).using(SizeTransform(false))
            }
        ) { target ->
            Box(modifier = Modifier.background(Color.Black).size(100.dp))
        }
    }
}