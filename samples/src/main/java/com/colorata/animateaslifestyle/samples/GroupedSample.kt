package com.colorata.animateaslifestyle.samples

import androidx.compose.foundation.background
import androidx.compose.foundation.gestures.Orientation
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.LazyRow
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.colorata.animateaslifestyle.material3.GroupedColumn
import com.colorata.animateaslifestyle.material3.GroupedRow
import com.colorata.animateaslifestyle.material3.ItemPosition
import com.colorata.animateaslifestyle.material3.groupedItems

@Preview
@Composable
fun GroupedListSample() {
    AnimationSample<Float> { _, _ ->
        /*LazyRow(Modifier.padding(10.dp)) {
            groupedItems(listOf(1, 2, 3, 4, 5), Orientation.Horizontal) {
                Box(
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(2.dp),
                    contentAlignment = Alignment.Center
                ) {
                    Text(text = it.toString())
                }
            }
        }*/
        GroupedRow(
            items = listOf(1, 2, 3, 4, 5, 6), modifier = Modifier
                .fillMaxSize()
                .padding(10.dp),
            horizontalArrangement = Arrangement.spacedBy(2.dp),
            outerCorner = 20.dp
        ) {
            Box(
                modifier = Modifier
                    .fillMaxHeight()
                    .padding(2.dp),
                contentAlignment = Alignment.Center
            ) {
                Text(text = it.toString())
            }
        }
    }
}