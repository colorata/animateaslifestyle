package com.colorata.animateaslifestyle.samples

import androidx.compose.animation.core.Animatable
import androidx.compose.animation.core.AnimationSpec
import androidx.compose.animation.core.tween
import androidx.compose.foundation.Canvas
import androidx.compose.foundation.background
import androidx.compose.foundation.isSystemInDarkTheme
import androidx.compose.foundation.layout.*
import androidx.compose.material3.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.graphics.PointMode
import androidx.compose.ui.graphics.StrokeCap
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.colorata.animateaslifestyle.*
import com.colorata.animateaslifestyle.stagger.*
import kotlinx.coroutines.delay
import kotlin.math.abs


@Composable
fun DotsBackground(size: Dp = 200.dp, content: @Composable () -> Unit) {
    val onSurface = MaterialTheme.colorScheme.onSurface
    Box(contentAlignment = Alignment.Center, modifier = Modifier.size(size)) {
        Canvas(
            modifier = Modifier
                .fillMaxSize()
                .background(MaterialTheme.colorScheme.surface)
        ) {
            val step = 20
            val offsets = mutableStateListOf<Offset>().apply {
                for (i in step..(this@Canvas.size.width.toInt() - step) step step) {
                    for (j in step..(this@Canvas.size.height.toInt() - step) step step) {
                        add(Offset(i.toFloat(), j.toFloat()))
                    }
                }
            }
            drawPoints(offsets, pointMode = PointMode.Points, color = onSurface)
        }
        content()
    }
}


@Composable
fun <T> AnimationSample(
    withSlowMotion: Boolean = true,
    content: @Composable (Boolean, AnimationSpec<T>) -> Unit
) {
    var animationSpec by remember { mutableStateOf(tween<T>(500)) }
    var inSlowMotion by remember { mutableStateOf(false) }
    var visible by remember { mutableStateOf(false) }
    LaunchedEffect(key1 = true) {
        while (true) {
            animationSpec = tween(500)
            visible = true
            delay(1000)
            visible = false
            delay(1000)
            inSlowMotion = true
            animationSpec = tween(2000)
            visible = true
            delay(1000)
            visible = false
            inSlowMotion = false
            delay(1000)
        }
    }
    MaterialTheme(if (isSystemInDarkTheme()) darkColorScheme() else lightColorScheme()) {
        DotsBackground {
            Box(modifier = Modifier.fillMaxSize(), contentAlignment = Alignment.Center) {
                content(visible, animationSpec)
                CompositionLocalProvider(LocalContentColor provides MaterialTheme.colorScheme.onSurface) {
                    Text(
                        text = "Slow motion", modifier = Modifier
                            .animateVisibility(
                                if (withSlowMotion) inSlowMotion else false,
                                fade()
                            )
                            .align(Alignment.BottomCenter)
                            .padding(20.dp),
                        fontSize = 10.sp
                    )
                }
            }
        }
    }
}

@Composable
fun FloatAnimationSample(
    range: ClosedFloatingPointRange<Float>,
    withSlowMotion: Boolean = true,
    content: @Composable (Float) -> Unit
) {
    val floatAnim = remember { Animatable(range.start) }
    var inSlowMotion by remember { mutableStateOf(false) }
    LaunchedEffect(key1 = true) {
        while (true) {
            floatAnim.animateTo(range.endInclusive, tween(500))
            delay(1000)
            floatAnim.animateTo(range.start, tween(500))
            delay(1000)
            inSlowMotion = true
            floatAnim.animateTo(range.endInclusive, tween(2000))
            delay(1000)
            floatAnim.animateTo(range.start, tween(2000))
            inSlowMotion = false
            delay(1000)
        }
    }
    MaterialTheme(if (isSystemInDarkTheme()) darkColorScheme() else lightColorScheme()) {
        DotsBackground {
            Box(modifier = Modifier.fillMaxSize()) {
                Row(
                    modifier = Modifier.fillMaxSize(),
                    horizontalArrangement = Arrangement.SpaceEvenly,
                    verticalAlignment = Alignment.CenterVertically
                ) {
                    val percent =
                        abs((floatAnim.value - range.start) / (range.endInclusive - range.start))
                    val color = MaterialTheme.colorScheme.primaryContainer
                    content(floatAnim.value)
                    Column(horizontalAlignment = Alignment.CenterHorizontally) {
                        Text(text = range.endInclusive.toString(), fontSize = 14.sp)
                        Spacer(modifier = Modifier.height(5.dp))
                        Canvas(
                            modifier = Modifier
                                .height(100.dp)
                                .width(10.dp)
                        ) {
                            drawLine(
                                color,
                                Offset(size.width / 2, size.height),
                                Offset(size.width / 2, size.height - size.height * percent),
                                strokeWidth = 5f,
                                cap = StrokeCap.Round
                            )
                            drawCircle(
                                color,
                                radius = size.width / 2,
                                center = Offset(size.width / 2, size.height - size.height * percent)
                            )
                        }
                        Spacer(modifier = Modifier.height(5.dp))
                        Text(text = range.start.toString(), fontSize = 14.sp)
                    }
                }
                CompositionLocalProvider(LocalContentColor provides MaterialTheme.colorScheme.onSurface) {
                    Text(
                        text = "Slow motion", modifier = Modifier
                            .animateVisibility(
                                if (withSlowMotion) inSlowMotion else false,
                                fade()
                            )
                            .align(Alignment.BottomCenter)
                            .padding(20.dp),
                        fontSize = 10.sp
                    )
                }
            }
        }
    }
}

/*
@Preview
@Composable
fun VisibilitySample() {
    AnimationSample { visible, spec ->
        Button(
            onClick = { */
/*TODO*//*
 },
            modifier = Modifier.animateVisibility(visible, fade() + slide(animationSpec = spec))
        ) {
            Text(text = "Send")
        }
    }
}
*/

@OptIn(ExperimentalStaggerApi::class)
@Preview
@Composable
fun LazyListSample() {
    val ripple = remember { staggerListOf({ Animatable(80f) }, true, 0, 1, 2) }
    LaunchedEffect(key1 = true) {
        ripple.animateAsList(this, spec = staggerSpecOf {
            animationValue.animateTo(0f)
        })
    }
    AnimationSample<Float> { visible, spec ->
        Row(
            modifier = Modifier
                .fillMaxWidth()
                .height(100.dp)
        ) {
            ripple.forEach {
                Box(
                    modifier = Modifier
                        .graphicsOffset(y = it.animationValue.value.dp.toPx())
                        .size(50.dp)
                        .background(MaterialTheme.colorScheme.primary)
                ) {
                    Text(text = it.value.toString())
                }
            }
        }
    }
}