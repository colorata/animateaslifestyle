package com.colorata.animateaslifestyle.samples

import android.content.res.Configuration.UI_MODE_NIGHT_YES
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.colorata.animateaslifestyle.stagger.ExperimentalStaggerApi
import com.colorata.animateaslifestyle.material3.MaterialAnimations
import com.colorata.animateaslifestyle.stagger.*
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

@OptIn(ExperimentalStaggerApi::class)
@Preview(uiMode = UI_MODE_NIGHT_YES)
@Composable
fun StaggerSample() {
    val items = remember { mutableStaggerListOf({ 0f }, false, "A", "B", "C") }
    AnimationSample<Float> { visible, spec ->
        LaunchedEffect(key1 = true) {
            items.animateAsList(this, spec = staggerSpecOf {
                this.visible = true
            })
        }
        LaunchedEffect(key1 = true) {
            launch {
                while (true) {
                    delay(3000)
                    /*if (items.any { it.value == "D" }) items.remove(
                        "D",
                        spec = staggerSpecOf(100, 300) {
                            this.visible = false
                        }) else {
                        items.add("D", { 0f }, spec = staggerSpecOf(100) {
                            this.visible = true
                        })
                    }*/
                    items.removeAt(0, spec = staggerSpecOf(100, 300) {
                        this.visible = false
                    })
                }
            }
        }
        LazyColumn(modifier = Modifier.fillMaxSize()) {
            staggerItems(
                items,
                transition = {
                    MaterialAnimations.sharedAxisY(it.visible)
                }) {
                Box(
                    modifier = Modifier
                        .clip(CircleShape)
                        .background(MaterialTheme.colorScheme.primary)
                        .fillMaxWidth(), contentAlignment = Alignment.Center
                ) {
                    Text(
                        text = it.value,
                        modifier = Modifier.padding(10.dp),
                        color = MaterialTheme.colorScheme.onPrimary
                    )
                }
            }
        }
    }
}